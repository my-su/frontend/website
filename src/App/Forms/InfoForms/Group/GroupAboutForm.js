import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";
import { ValidatorForm } from "react-material-ui-form-validator";

import Button from "../../../Components/Buttons/Button";
import {FileFieldV2} from "../../../Components/FormComponents/Fields/FileField";
import {TextFieldV3} from "../../../Components/FormComponents/Fields/TextField";


const GroupAboutForm = ({ onSubmit, onDelete, onCancel, enableDelete, group, parentable_groups, handleGroupChange }) => {
    let bodyText = "body1";

    return (
        <ValidatorForm
            onSubmit={onSubmit}
            onError={errors => console.log(errors)}
        >
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={2}>
                            <Typography>Image</Typography>
                        </Grid>
                        <Grid item>
                            <FileFieldV2
                                label={"Image"}
                                onChange={(group)=>handleGroupChange("photo",group.target.value)}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <TextFieldV3
                        multiline
                        labelXs={2}
                        variant={"separated"}
                        label={"Description"}
                        value={group.description}
                        onChange={(group)=>handleGroupChange("description",group.target.value)}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={2} justifyContent={"flex-end"}>
                        <Grid item>
                            <Button onClick={onCancel} version={"cancel"}>Cancel</Button>
                        </Grid>
                        <Grid item>
                            <Button type={"submit"} version={"save"}>Save</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </ValidatorForm>
    );
};

GroupAboutForm.propTypes = {
    group: PropTypes.object.isRequired,
    handleGroupChange: PropTypes.func.isRequired,
    // parentable_groups: PropTypes.arrayOf(PropTypes.object).isRequired,
    enableDelete: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
};

GroupAboutForm.defaultProps = {
};

export default GroupAboutForm;