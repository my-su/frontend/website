import PropTypes from "prop-types";
import React from "react";
import {ValidatorForm} from "react-material-ui-form-validator";

import Button from "../../../Components/Buttons/Button";
import SpecificDataField from "../../../Components/FormComponents/Fields/SpecificDataField";
import Wrapper from "../../../Components/FormComponents/Wrapper";
import {useGetDataFieldsByAssociationQuery} from "../../../Store/services/MySU/dataFields";


const SpecificDataForm = ({ specificData, handleSpecificDataChange, associationSlug, isBoard, onSubmit, onCancel}) => {
    const { data: dataFields } = useGetDataFieldsByAssociationQuery(associationSlug);

    const dataFieldUrlToSpecificDataValue = (url) => specificData?.find(data=> data.data_field === url)?.value || "";

    return (
        <ValidatorForm
            onSubmit={onSubmit}
            onError={errors => console.log(errors)}
        >
            { dataFields && dataFields.map((dataField, d)=>(
                <Wrapper key={d}>
                    <SpecificDataField
                        field={dataField}
                        helperText={dataField.helper_text}
                        value={dataFieldUrlToSpecificDataValue(dataField.url)}
                        onChange={handleSpecificDataChange}
                        disabled={(dataField.board_only || (dataField.mandatory && dataFieldUrlToSpecificDataValue(dataField.url) === "True")) && !isBoard}
                    />
                </Wrapper>
            )) }
            <Wrapper>
                <div/>
                <div>
                    <Button variant={"contained"} onClick={onCancel} color={"secondary"}>Cancel</Button>
                    &nbsp;
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Save</Button>
                </div>
            </Wrapper>
        </ValidatorForm>
    );
};

SpecificDataForm.propTypes = {
    specificData: PropTypes.array.isRequired,
    associationSlug: PropTypes.string.isRequired,
    isBoard: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    handleSpecificDataChange: PropTypes.func.isRequired
};

export default SpecificDataForm;