import PropTypes from "prop-types";
import React from "react";

import withExtraProps from "../../../Components/HOC/withExtraProps";
import withPropMap from "../../../Components/HOC/withPropMap";
import {usePatchSpecificDataMutation} from "../../../Store/services/MySU/members";
import {usePatchMySpecificDataMutation} from "../../../Store/services/MySU/user";
import InfoForm from "../InfoForm";
import SpecificDataForm from "./SpecificDataForm";
import SpecificDataInfo from "./SpecificDataInfo";


const formObjectToSpecificDataFormPropsMap = ({ formObject, handleFormObjectChange, ...rest_props}) =>
    ({ specificData: Object.values(formObject || {}), handleSpecificDataChange: handleFormObjectChange, ...rest_props});
const formObjectToSpecificDataInfoPropsMap = ({ info, ...rest_props}) =>
    ({ specificData: Object.values(info || {}), ...rest_props});

const SpecificDataInfoForm = ({ specificData, showInfoFormStateButton, initialInfoOrFormState, MoreActionButtonGroup, postSubmit, postCancel, isYou, isBoard, associationSlug }) => {
    const [ patchMySpecificData ] = usePatchMySpecificDataMutation();
    const [ patchSpecificData ] = usePatchSpecificDataMutation();

    const fieldAndValueToStateChanges = (field, value) => {
        const index=specificData?.findIndex(data=>data.data_field === field);
        return { [index]: { ...specificData[index], value: value, hasBeenChanged: true} };
    };
    const onSubmit = (submittedSpecificData) => {
        const changedSpecificData = Object.values(submittedSpecificData).filter(data=> data.hasBeenChanged);
        if (isYou) {
            changedSpecificData.forEach(changedData=>patchMySpecificData(changedData));
        } else {
            changedSpecificData.forEach(changedData=>patchSpecificData({ ...changedData, associationSlug: associationSlug}));
        }
    };
    const SpecificDataInfoComponent = withExtraProps(withPropMap(SpecificDataInfo, formObjectToSpecificDataInfoPropsMap), {
        associationSlug: associationSlug
    });
    const SpecificDataFormComponent = withExtraProps(withPropMap(SpecificDataForm, formObjectToSpecificDataFormPropsMap), {
        associationSlug: associationSlug,
        isBoard: isBoard,
    });

    return (
        <InfoForm
            onSubmit={onSubmit}
            onCancel={postCancel}
            title={"Data fields"}
            infoFormObject={specificData}
            InfoComponent={SpecificDataInfoComponent}
            FormComponent={SpecificDataFormComponent}
            fieldAndValueToStateChanges={fieldAndValueToStateChanges}
            showInfoFormStateButton={showInfoFormStateButton}
            initialInfoOrFormState={initialInfoOrFormState}
            MoreActionButtonGroup={MoreActionButtonGroup}
        />
    );
};

const dummyFunction = () => {};

SpecificDataInfoForm.propTypes = {
    specificData: PropTypes.object.isRequired,
    showInfoFormStateButton: PropTypes.bool.isRequired,
    initialInfoOrFormState: PropTypes.string.isRequired,
    MoreActionButtonGroup: PropTypes.elementType,
    enableDelete: PropTypes.bool,
    postSubmit: PropTypes.func,
    postDelete: PropTypes.func,
    postCancel: PropTypes.func,
    isYou: PropTypes.bool.isRequired,
    isBoard: PropTypes.bool.isRequired,
    associationSlug: PropTypes.string.isRequired
};

SpecificDataInfoForm.defaultProps = {
    enableDelete: false,
    MoreActionButtonGroup: dummyFunction(),
    postSubmit: dummyFunction,
    postDelete: dummyFunction,
    postCancel: dummyFunction,
};

export default SpecificDataInfoForm;