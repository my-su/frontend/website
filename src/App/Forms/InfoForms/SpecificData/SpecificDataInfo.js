import PropTypes from "prop-types";
import React, {useMemo} from "react";

import {useGetDataFieldsByAssociationQuery} from "../../../Store/services/MySU/dataFields";
import Info from "../Info";

const SpecificData = ({ specificData, association }) => {
    const { data: dataFields } = useGetDataFieldsByAssociationQuery(association.slug);
    const dataFieldToTypeDict = Object.fromEntries(dataFields?.map(dataField=>[dataField.name, dataField.type]) || []);

    const data = useMemo(()=>
        Object.fromEntries(specificData.map(sp=>[sp.name, dataFieldToTypeDict[sp.name] === "Boolean"
                ? (sp.value === "True" ? "Yes" : "No")
                : sp.value
        ]) || [])
    ,[specificData, dataFieldToTypeDict]);

    return (
        <Info
            headerless={true}
            data={data}
        />
    );
};

SpecificData.propTypes = {
    specificData: PropTypes.array.isRequired,
    association: PropTypes.object.isRequired
};

export default SpecificData;