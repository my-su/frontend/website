import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";
import {ValidatorForm} from "react-material-ui-form-validator";

import Button from "../../../Components/Buttons/Button";
import {FileFieldV2} from "../../../Components/FormComponents/Fields/FileField";
import {TextFieldV2, TextFieldV3, TextFieldV4} from "../../../Components/FormComponents/Fields/TextField";
import useAddValidationRule from "../../../Components/Hooks/useAddValidationRule";
import isIban from "../../../Components/ValidatorRules/isIban";

const AssociationAboutForm = ({ association, handleAssociationChange, onSubmit, onCancel }) => {
    useAddValidationRule(ValidatorForm, isIban.name, isIban.validate);

    return (
        <ValidatorForm
            onSubmit={onSubmit}
            onError={errors => console.log(errors)}
        >
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Grid container spacing={2} alignItems={"center"}>
                        <Grid item xs={2}>
                            <Typography variant={"body1"}>Photo</Typography>
                        </Grid>
                        <Grid item xs={10}>
                            <FileFieldV2
                                onChange={(event)=>handleAssociationChange("photo",event.target.files[0])}
                                name={"photo"}
                                size={"small"}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <TextFieldV3
                        labelXs={2}
                        variant={"separated"}
                        label={"Description"}
                        maxRows={10}
                        multiline
                        value={association.description}
                        onChange={(event)=>handleAssociationChange("description",event.target.value)}
                    />
                </Grid>
                <Grid item xs={12}>

                    <Grid container spacing={2} justifyContent={"flex-end"}>
                        <Grid item>
                            <Button onClick={onCancel} version={"cancel"}>Cancel</Button>
                        </Grid>
                        <Grid item>
                            <Button type={"submit"} version={"save"}>Save</Button>
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>

        </ValidatorForm>
    );
};

AssociationAboutForm.propTypes = {
    association: PropTypes.object.isRequired,
    handleAssociationChange: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
};

export default AssociationAboutForm;