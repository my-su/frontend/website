import {CircularProgress, Skeleton} from "@mui/material";
import MuiCardMedia from "@mui/material/CardMedia";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {isEmpty} from "lodash";
import React from "react";

import Image from "../../../../img/default_photo.jpg";
import Wrapper from "../../../Components/FormComponents/Wrapper";


const AssociationAboutInfo = ({ association }) => {
    if (isEmpty(association)) {
        return <Wrapper><CircularProgress /></Wrapper>;
    }
    return (
        // <Typography style={{whiteSpace: "pre-line"}}>
        //     { association.description }
        // </Typography>
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <MuiCardMedia image={association.photo || Image}
                    sx={{
                        width: "100%",
                        height: 0,
                        paddingBottom: "48%",
                        borderRadius: 4,
                        backgroundColor: "background.paper",
                        position: "relative",
                        transform: "translateY(8px)",
                        // paddingLeft: 1,
                        // paddingRight: 1,
                        "&:after": {
                            content: "\" \"",
                            position: "absolute",
                            top: 0,
                            left: 0,
                            width: "100%",
                            height: "100%",
                            borderRadius: "shape.borderRadius",
                            opacity: 0.5,
                        },
                    }}
                />
            </Grid>
            <Grid item xs={12}>
                <Typography variant={"body1"}>
                    { association.description }
                </Typography>
            </Grid>
        </Grid>
    );
};

export default AssociationAboutInfo;