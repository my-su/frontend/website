import moment from "moment";
import PropTypes from "prop-types";
import React from "react";

import Info from "../Info";


const EventInfo = ({ event }) => {
    const morphEventToData = (event) => {
        return {
            "Name": event.name,
            "Description": event.description,
            "From": moment(event.start_date).toLocaleString(),
            "Till": moment(event.end_date).toLocaleString(),
        };
    };

    return (
        <Info
            headerless={true}
            data={morphEventToData(event)}
        />
    );
};

EventInfo.propTypes = {
    event: PropTypes.object.isRequired
};

export default EventInfo;