import AddIcon from "@mui/icons-material/Add";
import BallotIcon from "@mui/icons-material/Ballot";
import CalendarToday from "@mui/icons-material/CalendarToday";
import Create from "@mui/icons-material/Create";
import LocationOn from "@mui/icons-material/LocationOn";
import Notes from "@mui/icons-material/Notes";
import RemoveIcon from "@mui/icons-material/Remove";
import WallpaperIcon from "@mui/icons-material/Wallpaper";
import {Dialog, DialogActions, DialogContent} from "@mui/material";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import MenuItem from "@mui/material/MenuItem";
import Switch from "@mui/material/Switch";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import * as moment from "moment/moment";
import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";
import {SelectValidator, ValidatorForm} from "react-material-ui-form-validator";

import Button from "../../../Components/Buttons/Button";
import DateTimeField, {
    DateTimeFieldV2,
    DateTimeFieldV3,
    DateTimeFieldV4
} from "../../../Components/FormComponents/Fields/DateTimeField";
import FileField, {FileFieldV2} from "../../../Components/FormComponents/Fields/FileField";
import NumberField from "../../../Components/FormComponents/Fields/NumberField";
import SelectField, {SelectFieldV3} from "../../../Components/FormComponents/Fields/SelectField";
import TextField, {TextFieldV2, TextFieldV3} from "../../../Components/FormComponents/Fields/TextField";
import FontAwesomeIconHolder from "../../../Components/FormComponents/FontAwesomeIconHolder";
import IconHolder from "../../../Components/FormComponents/IconHolder";
import Wrapper from "../../../Components/FormComponents/Wrapper";
import Divider from "../../../Components/PageLayout/Content/Divider";
import {fromAPItoSchedulerFormat} from "../../../Pages/Calendar/Scheduler";
import {useGetAssociationBySlugQuery} from "../../../Store/services/MySU/associations";
import EnrollmentOptions, {useGetEnrollmentOptionsByEventQuery} from "../../../Store/services/MySU/enrollmentOptions";
import {
    useAddEventMutation,
    useDeleteEventMutation,
    useGetEventQuery,
    usePatchEventMutation
} from "../../../Store/services/MySU/events";
import EnrollmentOptionsDropDown from "../../DropdownForms/EnrollmentOptions/EnrollmentOptionsDropDown";
import EnrollmentOptionsForm from "../../DropdownForms/EnrollmentOptions/EnrollmentOptionsForm";

const useStyles = makeStyles(() => ({
    actionWrapper: {
        width: "100%"
    }
}));

const fromSchedulertoAPIFormat = (calendarEvent, eventSlug) => {
    // API wants UTC ISOstrings, but scheduler gives local strings.
    let apiCalendarEvent = {
        ...calendarEvent,
        slug: eventSlug,
        start_date:  new Date(calendarEvent.startDate).toISOString(),
        end_date:  new Date(calendarEvent.endDate).toISOString(),
        name: calendarEvent.name,
    };
    if (calendarEvent.enrollable === true) {
        apiCalendarEvent["enrollable_until"] = new Date(apiCalendarEvent["enrollable_until"]).toISOString();
        apiCalendarEvent["enrollable_from"] = new Date(apiCalendarEvent["enrollable_from"]).toISOString();
    }
    if (apiCalendarEvent.unenrollable === true) {
        apiCalendarEvent["unenrollable_until"] = new Date(apiCalendarEvent["unenrollable_until"]).toISOString();
    }
    return apiCalendarEvent;
};



const EventModalForm = ({ visible, postCancel, postSubmit, postDelete, eventTypes, eventSlug, associationSlug}) => {
    const classes = useStyles();

    const baseEvent = {
        association: `/associations/${associationSlug}`,
        // enrollable: false,
        // unenrollable: false,
        // slug: "",
        // type: "",
    };

    const [ addEvent] = useAddEventMutation();
    const [ updateEvent ] = usePatchEventMutation();
    const [ removeEvent ] = useDeleteEventMutation();

    // const { data: event } = useGetEventQuery(eventSlug);
    // const selectedEvent = fromAPItoSchedulerFormat(event || {});
    // console.log("Slug" + event);
    // const [calendarEvent, setCalendarEvent] = useState({...selectedEvent});
    const { data: enrollmentOptions } = useGetEnrollmentOptionsByEventQuery(eventSlug);
    const [creating, setCreating] = useState(false);

    const { data: event, isLoading } = useGetEventQuery(eventSlug);

    const selectedEvent = fromAPItoSchedulerFormat(event || {"association": "/associations/" + eventSlug });

    const [calendarEventChanges, setCalendarEventChanges] = useState({});
    const calendarEvent = {...baseEvent, ...selectedEvent, ...calendarEventChanges};
    useEffect(() => {
        setCalendarEventChanges({...baseEvent, ...selectedEvent});
    }, [visible]);

    const handleSubmit = () => {
        if (eventSlug !== "new") {
            updateEvent(fromSchedulertoAPIFormat(calendarEvent, eventSlug)).then(()=>postSubmit());
        } else {
            addEvent(fromSchedulertoAPIFormat(calendarEvent)).then(()=>postSubmit());
        }
    };
    const onDelete = () => {
        removeEvent(fromSchedulertoAPIFormat(calendarEvent, eventSlug)).then(()=>postDelete());
    };


    const handleEventChange = (field, value) => {
        if (field === "public" && value) {
            setCalendarEventChanges(prevState => ({...prevState, public: value, enrollable: false, unenrollable: false}));
        } else if (field === "startDate") {
            setCalendarEventChanges(prevState => ({...prevState, startDate: value, endDate: moment(prevState.endDate).add(moment(value).diff(moment(prevState.startDate))).format("YYYY-MM-DD HH:mm")}));
            // } else if (field === "enrollable_from") {
            //     setCalendarEvent(prevState => ({...prevState, enrollable_from: value, enrollable_until: moment(prevState.enrollable_until).add(moment(value).diff(moment(prevState.enrollable_from)))}));
        } else if (field === "enrollable") {
            if (value) {
                setCalendarEventChanges(prevState => ({...prevState, enrollable: true, enrollable_from: moment().format("YYYY-MM-DD HH:mm"), enrollable_until: prevState.startDate}));
            } else {
                setCalendarEventChanges(prevState => ({...prevState, enrollable: false, enrollable_until: null, enrollable_from: null}));
            }
        } else if (field === "unenrollable") {
            if (value) {
                setCalendarEventChanges(prevState => ({...prevState, unenrollable: true, unenrollable_until: prevState.startDate}));
            } else {
                setCalendarEventChanges(prevState => ({...prevState, unenrollable: false, unenrollable_until: null}));
            }
        } else {
            setCalendarEventChanges(prevState => ({...prevState, [field]: value}));
        }
    };


    return (
        <Dialog
            open={visible}
            maxWidth={"sm"}
        >
            <ValidatorForm
                onSubmit={handleSubmit}
                onError={errors => console.log(errors)}
            >
                <DialogTitle>
                    {eventSlug === "new" ? "Create event" : "Change event"}
                </DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant={"body1"} color={"primary"}>Title event</Typography>
                            <TextFieldV3
                                name={"name"}
                                variant={"separated"}
                                labelXs={0}
                                sx={{width:"100%"}}
                                value={calendarEvent.name}
                                onChange={(event) => handleEventChange("name", event.target.value)}
                                validators={["minStringLength:4"]}
                                errorMessages={["Name must have at least 4 characters"]}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant={"body1"} color={"primary"}>Description</Typography>
                            <TextFieldV3
                                value={calendarEvent.description}
                                variant={"separated"}
                                labelXs={0}
                                onChange={(event) => handleEventChange("description", event.target.value)}
                                multiline
                                rows={"3"}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant={"body1"} color={"primary"}>Practical information</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <DateTimeFieldV4
                                variant={"separated"}
                                labelXs={6}
                                label={"From (date + time)"}
                                name={"Start date"}
                                value={calendarEvent.startDate}
                                onChange={date => handleEventChange("startDate", date)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <DateTimeFieldV4
                                variant={"separated"}
                                labelXs={6}
                                label={"End date"}
                                name={"End date"}
                                value={calendarEvent.endDate}
                                onChange={date => handleEventChange("endDate", date)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextFieldV3
                                variant={"separated"}
                                label={"Location"}
                                labelXs={6}
                                value={calendarEvent.location}
                                onChange={(event) => handleEventChange("location", event.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <SelectFieldV3
                                variant={"separated"}
                                label={"Type"}
                                labelXs={6}
                                value={calendarEvent.type}
                                validators={["required"]}
                                errorMessages={["A type has to be set"]}
                                onChange={(event) => handleEventChange("type", event.target.value)}
                                required
                            >
                                { eventTypes && eventTypes.map(event_type => (
                                    <MenuItem key={event_type.slug} value={event_type.url}>
                                        { event_type.type }
                                    </MenuItem>
                                )) }
                            </SelectFieldV3>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container spacing={2} alignItems={"center"}>
                                <Grid item xs={6}>
                                    <Typography variant={"body1"}>Event image</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <FileFieldV2
                                        onChange={(event)=>handleEventChange("image",event.target.files[0])}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>


                    </Grid>
                    <Divider/>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant={"body1"} color={"primary"}>Enrollment</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <Typography>Is enrollment still possible?</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                color={"primary"}
                                                value={calendarEvent.enrollable}
                                                checked={calendarEvent.enrollable}
                                                onChange={(event) => handleEventChange("enrollable", event.target.checked)}
                                            />
                                        }
                                        label={""}
                                        // labelPlacement={"top"}
                                        disabled={calendarEvent.public}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>


                    { calendarEvent.enrollable &&
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <DateTimeFieldV4
                                    variant={"separated"}
                                    labelXs={6}
                                    label={"Enrollable from"}
                                    name={"Enrollable from"}
                                    value={calendarEvent.enrollable_from}
                                    onChange={(date) => handleEventChange("enrollable_from", date)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <DateTimeFieldV4
                                    variant={"separated"}
                                    labelXs={6}
                                    label={"Enrollable until"}
                                    name={"Enrollable until"}
                                    value={calendarEvent.enrollable_until}
                                    onChange={(date) => handleEventChange("enrollable_until", date)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <NumberField
                                    labelXs={6}
                                    label={"Maximum number of enrollments"}
                                    name={"Maximum number of enrollments"}
                                    value={calendarEvent.max_number_of_enrollments}
                                    onChange={(event) => handleEventChange("max_number_of_enrollments", event.target.value)}
                                    InputProps={{
                                        inputProps: {
                                            step: 1, min: 0
                                        }
                                    }}
                                    variant={"separated"}
                                    // helperText={"A maximum of `0` means no limit to the number of enrollments"}
                                    required
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Typography>Is disenrollment possible?</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    color={"primary"}
                                                    value={calendarEvent.unenrollable}
                                                    checked={calendarEvent.unenrollable}
                                                    onChange={(event) => handleEventChange("unenrollable", event.target.checked)}
                                                />
                                            }
                                            label={""}
                                            disabled={!calendarEvent.enrollable}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>

                    }
                    { calendarEvent.unenrollable &&
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <DateTimeFieldV4
                                variant={"separated"}
                                labelXs={6}
                                label={"Unenrollable until"}
                                name={"Unenrollable until"}
                                value={calendarEvent.unenrollable_until}
                                onChange={(date) => handleEventChange("unenrollable_until", date)}
                            />
                        </Grid>
                    </Grid>
                    }
                    {/*{ calendarEvent.enrollable &&*/}
                    {/*<Grid container spacing={2}>*/}
                    {/*    <Grid item xs={12}>*/}
                    {/*        <Divider/>*/}
                    {/*    </Grid>*/}
                    {/*    <Grid item xs={12}>*/}
                    {/*        <Typography variant={"body1"} color={"primary"}>Enrollment options</Typography>*/}
                    {/*    </Grid>*/}
                    {/*    <Grid ite={6}>*/}
                    {/*        <Typography>Add enrollment options?</Typography>*/}
                    {/*    </Grid>*/}
                    {/*    <Grid item xs={6}>*/}
                    {/*        <FormControlLabel*/}
                    {/*            control={*/}
                    {/*                <Switch*/}
                    {/*                    color={"primary"}*/}
                    {/*                    value={enroll}*/}
                    {/*                    checked={calendarEvent.unenrollable}*/}
                    {/*                    onChange={(event) => handleEventChange("unenrollable", event.target.checked)}*/}
                    {/*                />*/}
                    {/*            }*/}
                    {/*            label={""}*/}
                    {/*            disabled={!calendarEvent.enrollable}*/}
                    {/*        />*/}
                    {/*    </Grid>*/}
                    {/*    <Grid item xs={12}>*/}
                    {/*        <EnrollmentOptionsDropDown*/}
                    {/*            associationUrl={`/associations/${associationSlug}`}*/}
                    {/*            eventUrl={`/events/${eventSlug}`}*/}
                    {/*            enrollmentOptions={enrollmentOptions || []}*/}
                    {/*            creating={creating}*/}
                    {/*            postSubmit={()=>setCreating(false)}*/}
                    {/*            postCancel={()=>setCreating(false)}*/}
                    {/*        />*/}

                    {/*        <Button onClick={()=>setCreating(true)} version={"cancel"}>*/}
                    {/*            New option*/}
                    {/*        </Button>*/}
                    {/*    </Grid>*/}

                    {/*</Grid>*/}
                    {/*}*/}
                    <Divider />
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography>Hide event for members?</Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        color={"primary"}
                                        value={calendarEvent.hidden_for_members}
                                        checked={calendarEvent.hidden_for_members}
                                        onChange={(event) => handleEventChange("hidden_for_members", event.target.checked)}
                                    />
                                }
                                label={""}
                            />
                        </Grid>

                    </Grid>

                </DialogContent>
                <DialogActions>
                    <Wrapper className={classes.actionWrapper}>
                        <div>
                            <Button version={"remove"} disabled={!selectedEvent.slug} onClick={onDelete}>
                                Delete
                            </Button>
                        </div>
                        <div>
                            <Button version={"cancel"} onClick={postCancel}>Cancel</Button>
                            &nbsp;
                            <Button version={"save"} type={"submit"} color={"primary"}>Save</Button>
                        </div>
                    </Wrapper>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    );
};

EventModalForm.propTypes = {
    visible: PropTypes.bool.isRequired,
    postCancel: PropTypes.func.isRequired,
    postSubmit: PropTypes.func.isRequired,
    postDelete: PropTypes.func.isRequired,
    eventTypes: PropTypes.array.isRequired,
    eventSlug: PropTypes.string.isRequired
};

export default EventModalForm;