import PropTypes from "prop-types";
import React, {useEffect, useMemo} from "react";

import ConfirmationModal, {ConfirmationModalV2} from "../../../../Components/Modals/ConfirmationModal";

const confirmButtonText = "Accept";
const secondaryButtonText = "Decline";
const cancelButtonText = "Cancel";

const MemberIncomingPopupForm = ({ row, onChange, onApplyChanges, onCancelChanges, open }) => {
    const description = useMemo(()=>"Do you want to accept or decline " + row.given_name + " " + row.surname +"?", [row.given_name, row.surname]);
    const title = "Incoming Member '" + row.given_name + " " + row.surname +"'";

    useEffect(()=>{
        if (row.status !== "Pending" && row.changed === true) {
            onApplyChanges();
        }
    }, [row.status, onApplyChanges]);

    const acceptMember = () => {
        onChange({ target: { name: "status", value: "Accepted" } });
        onChange({ target: { name: "changed", value: true } });
    };
    const rejectMember = () => {
        onChange({ target: { name: "status", value: "Rejected" } });
        onChange({ target: { name: "changed", value: true } });
    };

    return (
        <ConfirmationModalV2
            title={title}
            onCancel={onCancelChanges}
            open={open}
            onConfirm={acceptMember}
            description={description}
            size={"xs"}
            cancelButtonText={cancelButtonText}
            confirmButtonText={confirmButtonText}
            secondaryButtonText={secondaryButtonText}
            secondaryAction={rejectMember}
        />
    );
};

MemberIncomingPopupForm.propTypes = {
    row: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onApplyChanges: PropTypes.func.isRequired,
    onCancelChanges: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};

export default MemberIncomingPopupForm;