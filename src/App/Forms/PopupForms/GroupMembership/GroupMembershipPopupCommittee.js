/* eslint-disable no-shadow */
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Grid from "@mui/material/Grid";
import React, {useMemo} from "react";
import {ValidatorForm} from "react-material-ui-form-validator";
import {useParams} from "react-router-dom";

import Button from "../../../Components/Buttons/Button";
import {DateFieldV3} from "../../../Components/FormComponents/Fields/DateField";
import MemberField, {useMemberFieldOptions} from "../../../Components/FormComponents/Fields/MemberField";
import NumberField from "../../../Components/FormComponents/Fields/NumberField";
import TextField, {TextFieldV3} from "../../../Components/FormComponents/Fields/TextField";
import Wrapper from "../../../Components/FormComponents/Wrapper";


const PopupCommittee = ({ row, onChange, onApplyChanges, onCancelChanges, open }) => {
    const { slug } = useParams();
    const memberFieldOptions = useMemberFieldOptions(slug);
    const profile = useMemo(()=>memberFieldOptions.find(option=> option.url === row.profile), [memberFieldOptions, row.profile]);

    return (
        <Dialog open={open} onClose={onCancelChanges} aria-labelledby={"form-dialog-title"} fullWidth={true}>
            <ValidatorForm
                onSubmit={onApplyChanges}
                onError={errors => console.log(errors)}
                debounceTime={1000}
            >
                {/*<DialogTitle id={"form-dialog-title"}>{ profile ? "Edit member" : "New member" }</DialogTitle>*/}
                <DialogTitle id={"form-dialog-title"}>Committee member details</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <MemberField
                                associationSlug={slug}
                                size={"small"}
                                variant={"separated"}
                                name={"profile"}
                                label={"Member"}
                                value={profile || {url: ""}}
                                onChange={(event, value)=>onChange({target: {name: "profile", value: value ? value.url : ""}})}
                                disabled={Boolean(row.slug)}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextFieldV3
                                variant={"separated"}
                                name={"duty"}
                                label={"Duty"}
                                value={row.duty || ""}
                                onChange={onChange}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <DateFieldV3
                                variant={"separated"}
                                name={"date_joined"}
                                label={"Date joined"}
                                value={row.date_joined}
                                onChange={(date)=>onChange({ target: {name: "date_joined", value: date}})}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <DateFieldV3
                                variant={"separated"}
                                name={"date_left"}
                                label={"Date left"}
                                value={row.date_left || null}
                                onChange={(date)=>onChange({ target: {name: "date_left", value: date}})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <NumberField
                                name={"order"}
                                label={"Order"}
                                value={row.order || ""}
                                onChange={onChange}
                                InputProps={{
                                    inputProps: {
                                        step: 1, min: 1
                                    }
                                }}
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid
                                container
                                spacing={2}
                                direction={"row"}
                                justifyContent={"flex-end"}
                                alignItems={"center"}
                            >
                                <Grid item xs={12}>
                                    <Grid container spacing={2} justifyContent={"flex-end"}>
                                        <Grid item>
                                            <Button onClick={onCancelChanges} version={"cancel"}>Cancel</Button>
                                        </Grid>
                                        <Grid item>
                                            <Button type={"submit"} version={"save"}>Save</Button>
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>
            </ValidatorForm>
        </Dialog>
    );
};

export default PopupCommittee;