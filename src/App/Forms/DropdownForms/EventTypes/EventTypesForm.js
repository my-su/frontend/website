import ContactsIcon from "@mui/icons-material/Contacts";
import PropTypes from "prop-types";
import React from "react";
import {ValidatorForm} from "react-material-ui-form-validator";

import Button from "../../../Components/Buttons/Button";
import {TextFieldV2, TextFieldV3} from "../../../Components/FormComponents/Fields/TextField";
import IconHolder from "../../../Components/FormComponents/IconHolder";
import Wrapper from "../../../Components/FormComponents/Wrapper";

const EventTypesForm = ({ eventType, handleEventTypeChange, onSubmit, onCancel, onDelete, enableDelete }) => {

    return (
        <ValidatorForm
            onSubmit={onSubmit}
            onError={errors => console.log(errors)}
        >
            <Wrapper>
                <TextFieldV3
                    variant={"separated"}
                    name={"type"}
                    label={"Type"}
                    value={eventType.type}
                    onChange={(event)=>handleEventTypeChange("type", event.target.value)}
                />
            </Wrapper>
        </ValidatorForm>
    );
};

EventTypesForm.propTypes = {
    eventType: PropTypes.object.isRequired,
    handleEventTypeChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    enableDelete: PropTypes.bool.isRequired
};

export default EventTypesForm;