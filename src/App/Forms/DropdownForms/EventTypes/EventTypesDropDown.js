import PropTypes from "prop-types";
import React from "react";

import withPropMap from "../../../Components/HOC/withPropMap";
import {
    useAddEventTypeMutation,
    useDeleteEventTypeMutation,
    usePatchEventTypeMutation
} from "../../../Store/services/MySU/eventTypes";
import MultiFormContainer from "../../MultiForms/MultiForm";
import DropDownFormContainer from "../dropDownForm";
import EventTypeInfo from "./EventTypeInfo";
import EventTypesForm from "./EventTypesForm";

const dropDownObjectToEventTypesFormPropsMap = ({ multiObject, handleMultiObjectChange, ...rest_props}) =>
    ({ eventType: multiObject, handleEventTypeChange: handleMultiObjectChange, ...rest_props});
const multiObjectToEventTypeInfoPropsMap = ({ multiObject, ...rest_props }) =>
    ({ eventType: multiObject, ...rest_props});

const eventTypeToTitle = (eventType) => eventType.type;
const fieldAndValueToStateChanges = (field, value) => ({ [field]: value} );

const EventTypesDropDown = ({ eventTypes, associationUrl, postSubmit, postCancel, postDelete, creating }) => {
    const initialEventType = { association: associationUrl };
    const EventTypeFormComponent = withPropMap(EventTypesForm, dropDownObjectToEventTypesFormPropsMap);
    
    const [ addEventType ] = useAddEventTypeMutation();
    const [ patchEventType ] = usePatchEventTypeMutation();
    const [ removeEventType ] = useDeleteEventTypeMutation();

    const onSubmit = (eventType) => eventType.slug
        ? patchEventType(eventType).then(()=>postSubmit(eventType, eventTypes))
        : addEventType(eventType).then(()=>postSubmit(eventType, eventTypes));
    const onDelete = (eventType) => removeEventType(eventType).then(()=>postDelete(eventType, eventTypes));

    return (
        <DropDownFormContainer
            creating={creating}
            dropDownObjects={eventTypes}
            dropDownObjectToTitle={eventTypeToTitle}
            allowCreation={true}
            initialDropDownObject={initialEventType}
            FormComponent={EventTypeFormComponent}
            fieldAndValueToStateChanges={fieldAndValueToStateChanges}
            enableDelete={true}
            onSubmit={onSubmit}
            onCancel={postCancel}
            onDelete={onDelete}
        />
    );
};

const EventTypesMultiForm = ({ eventTypes, associationUrl, postSubmit, postCancel, postDelete, creating }) => {
    const initialEventType = { association: associationUrl };
    const EventTypeFormComponent = withPropMap(EventTypesForm, dropDownObjectToEventTypesFormPropsMap);
    const EventTypeInfoComponent = withPropMap(EventTypeInfo, multiObjectToEventTypeInfoPropsMap);

    const [ addEventType ] = useAddEventTypeMutation();
    const [ patchEventType ] = usePatchEventTypeMutation();
    const [ removeEventType ] = useDeleteEventTypeMutation();

    const onSubmit = (eventType) => eventType.slug
        ? patchEventType(eventType).then(()=>postSubmit(eventType, eventTypes))
        : addEventType(eventType).then(()=>postSubmit(eventType, eventTypes));
    const onDelete = (eventType) => removeEventType(eventType).then(()=>postDelete(eventType, eventTypes));

    return (
        <MultiFormContainer
            initialMultiObject={initialEventType}
            multiObjectToTitle={eventTypeToTitle}
            allowCreation={true}
            multiObjects={eventTypes}
            title={"Event types"}
            InfoComponent={EventTypeInfoComponent}
            FormComponent={EventTypeFormComponent}
            fieldAndValueToStateChanges={fieldAndValueToStateChanges}
            enableDelete={false}
            creating={creating}
            onSubmit={onSubmit}
            onCancel={postCancel}
            onDelete={onDelete}
        />
    );
};

EventTypesDropDown.propTypes = {
    eventTypes: PropTypes.array.isRequired,
    associationUrl: PropTypes.string.isRequired,
    postSubmit: PropTypes.func,
    postCancel: PropTypes.func,
    postDelete: PropTypes.func
};

const dummyFunction = () => {};
EventTypesDropDown.defaultProps = {
    postSubmit: dummyFunction,
    postCancel: dummyFunction, 
    postDelete: dummyFunction
};

export {EventTypesMultiForm};

export default EventTypesDropDown;