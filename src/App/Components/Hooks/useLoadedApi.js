import PropTypes from "prop-types";
import {useEffect} from "react";

const useLoadedApi = ( api ) => {
    useEffect(()=>{
        if (!api.loaded) {
            api.refresh();
        }
    }, [api.loaded]);

    return api;
};

useLoadedApi.propTypes = {
    api: PropTypes.object.isRequired
};

export default useLoadedApi;