import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";

import EventInfo from "../../Forms/InfoForms/Event/EventInfo";
import {EventForm} from "../Forms/EventForm";


const Event = ({association, event: propEvent, infoOrForm, ...props}) => {
    const [event, setEvent] = useState(propEvent.association ? propEvent : {...propEvent, association: association.url});

    useEffect(()=>setEvent(propEvent), [propEvent]);

    const handleEventChange = (field, value) => {
        setEvent(prevState => ({...prevState, [field]:value}));
    };

    if (infoOrForm === "info") {
        return (
            <EventInfo
                event={event}
            />
        );
    } else {
        return (
            <EventForm
                association={association}
                event={event}
                handleEventChange={handleEventChange}
                {...props}
            />
        );
    }
};

Event.propTypes = {
    infoOrForm: PropTypes.string.isRequired,
    event: PropTypes.object
};

Event.defaultProps = {
    event: {
        association: null,
        name: "",
        description: "",
        start_date: null,
        end_date: null,
        type: null,
        photo: null,
        public: null,
        enrollable_from: null,
        enrollable_until: null,
        unenrollable_until: null
    }
};

export default Event;
