import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import GroupWorkIcon from "@mui/icons-material/GroupWork";
import TreeItem from "@mui/lab/TreeItem";
import TreeView from "@mui/lab/TreeView";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React from "react";


const useStyles = makeStyles(theme=>({
    root: {
        color: theme.palette.text.secondary,
        "&:hover > $content": {
            backgroundColor: theme.palette.action.hover,
        },
        "&:focus > $content, &$selected > $content": {
            backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
            color: "var(--tree-view-color)",
        },
        "&:focus > $content $label, &:hover > $content $label, &$selected > $content $label": {
            backgroundColor: "transparent",
        },
    },
    content: {
        color: theme.palette.text.secondary,
        borderTopRightRadius: theme.spacing(2),
        borderBottomRightRadius: theme.spacing(2),
        paddingRight: theme.spacing(1),
        fontWeight: theme.typography.fontWeightMedium,
        "$expanded > &": {
            fontWeight: theme.typography.fontWeightRegular,
        },
    },
    group: {
        marginLeft: 0,
        "& $content": {
            paddingLeft: theme.spacing(2),
        },
    },
    expanded: {},
    selected: {},
    label: {
        fontWeight: "inherit",
        color: "inherit",
    },
    labelRoot: {
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(0.5, 0),
    },
    labelIcon: {
        marginRight: theme.spacing(1),
    },
    labelText: {
        fontWeight: "inherit",
        flexGrow: 1,
    },
}));

const RecursiveTreeView = ({ nodes }) => {
    const classes = useStyles();

    const renderTree = (nodes, id) => {
        const Icon = nodes.LabelIcon || GroupWorkIcon;
        return (
            <TreeItem
                key={id}
                nodeId={nodes.id}
                label={
                    <div className={classes.labelRoot}>
                        <Icon color={"inherit"} className={classes.labelIcon} />
                        <Typography variant={"body2"} className={classes.labelText}>
                            { nodes.primary }
                        </Typography>
                        <Typography variant={"caption"} color={"inherit"}>
                            { nodes.secondary }
                        </Typography>
                    </div>
                }
            >
                { (Array.isArray(nodes.children) && nodes.children.length > 0)
                    ? nodes.children.map((node, n) => renderTree(node, n))
                    : null
                }
            </TreeItem>
        );
    };

    return (
        <TreeView
            className={classes.root}
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpanded={["root"]}
            defaultExpandIcon={<ChevronRightIcon />}
        >
            { Array.isArray(nodes) ? nodes.map((node, n)=>renderTree(node, n)): renderTree(nodes) }
        </TreeView>
    );
};

RecursiveTreeView.propTypes = {
    nodes: PropTypes.oneOfType([PropTypes.object, PropTypes.arrayOf(PropTypes.object)])
};

RecursiveTreeView.defaultProps = {
    nodes: {
        id: "root",
        text: "Parent",
        LabelIcon: GroupWorkIcon,
        children: [
            {
                id: "1",
                text: "Child - 1",
                LabelIcon: GroupWorkIcon,
            },
            {
                id: "3",
                text: "Child - 3",
                LabelIcon: GroupWorkIcon,
                children: [
                    {
                        id: "4",
                        text: "Child - 4",
                        LabelIcon: GroupWorkIcon,
                    },
                ],
            },
        ],
    }
};

export default RecursiveTreeView;