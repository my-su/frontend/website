// import "react-table/react-table.css";

import DeleteIcon from "@mui/icons-material/Delete";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import { lighten } from "@mui/material/styles";
import MuiTable from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import MuiToolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import clsx from "clsx";
import PropTypes from "prop-types";
import React, { useState } from "react";

import { Helper } from "../../Helper";


const useToolbarStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.mode === "light"
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: "1 1 100%",
    },
}));

const EnhancedTableToolbar = (props) => {
    const classes = useToolbarStyles();
    const { numSelected } = props;

    return (
        <MuiToolbar
            className={clsx(classes.root, {[classes.highlight]: numSelected > 0})}
        >
            { numSelected > 0 ? (
                <Typography className={classes.title} color={"inherit"} variant={"subtitle1"} component={"div"}>
                    { numSelected } selected
                </Typography>
            ) : (
                <Typography className={classes.title} variant={"h6"} id={"tableTitle"} component={"div"}>
                    Rows
                </Typography>
            ) }

            { numSelected > 0 && (
                <Tooltip title={"Delete"}>
                    <IconButton aria-label={"delete"} onClick={()=>props.onClick()} size={"large"}>
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            ) }
        </MuiToolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


const useStyles = makeStyles(theme => ({
    wrapper: {
        display: "flex",
        justifyContent: "space-between",
        padding: theme.spacing(1, 0),
    },
    icon: {
        margin: theme.spacing(2, 0),
        marginRight: theme.spacing(2),
    },
    textField: {
        width: "100%",
    },
    column: {
        width: "48%"
    },
    matchButtonDiv: {
        display: "flex",
        justifyContent: "center",
    },
    matchButton: {
        width: "48%",
        fontSize: "24px",
    }
}));

const EnhancedTable = ({headers, filterProperties, rows, ...props}) => {
    const classes = useStyles();

    const [selected, setSelected] = useState([]);
    const [allSelected, setAllSelected] = useState(false);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [page, setPage] = useState(0);


    const handleCheck = (event, row_slug) => {
        // Function adds the row to the list of checked rows if not checked yet, else removes it from the checked rows
        let new_selected = selected.filter(slug=>slug!==row_slug);
        // check if filtering removed an element, which meant that the row_slug was in there
        if (new_selected.length === selected.length) {
            // pushing to selected and then calling setSelected gives state errors, so doing the concat as workaround
            new_selected = new_selected.concat([row_slug]);
            setSelected(new_selected);
        } else {
            setSelected(new_selected);
        }

        if (new_selected.length === rows.length) {
            setAllSelected(true);
        } else {
            setAllSelected(false);
        }
    };

    const handleAllCheck = () => {
        let newAllSelected = !allSelected;
        if (newAllSelected) {
            setSelected(rows.map(row=>row.slug));
        } else {
            setSelected([]);
        }
        setAllSelected(newAllSelected);
    };

    const handleDelete = () => {
        let deleted_rows = rows.filter(row=>selected.includes(row.slug));
        let new_rows = rows.filter(row=>!selected.includes(row.slug));
        setAllSelected(false);
        setSelected([]);
        props.onDelete(deleted_rows, new_rows);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <Paper>
            <EnhancedTableToolbar
                numSelected={selected.length}
                onClick={handleDelete}
            />
            <TableContainer component={Paper}>
                <MuiTable className={classes.table} size={"small"} aria-label={"a dense table"}>
                    <TableHead>
                        <TableRow>
                            <TableCell padding={"checkbox"}>
                                <Checkbox
                                    checked={allSelected}
                                    onChange={handleAllCheck}
                                    inputProps={{ "aria-label": "select all desserts" }}
                                />
                            </TableCell>
                            { headers.map((header, h)=><TableCell key={h}>{ header }</TableCell>) }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        { rows.map((row, i) =>{
                            let checked = selected.includes(row.slug);
                            return (
                                <TableRow
                                    key={i}
                                    role={"checkbox"}
                                    aria-checked={checked}
                                    selected={checked}
                                    onClick={(event) => handleCheck(event, row.slug)}
                                >
                                    <TableCell padding={"checkbox"}>
                                        <Checkbox
                                            checked={checked}
                                            inputProps={{ "aria-labelledby": i }}
                                        />
                                    </TableCell>
                                    { Object.values(Helper.filterObjectOnProperties(row, filterProperties)).map((column, c)=><TableCell key={c}>{ column }</TableCell>) }
                                </TableRow>
                            );}) }
                    </TableBody>
                </MuiTable>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component={"div"}
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    );
};

export default EnhancedTable;