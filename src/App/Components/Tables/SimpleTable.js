import Paper from "@mui/material/Paper";
import MuiTable from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React from "react";

const useStyles = makeStyles(theme => ({
    wrapper: {
        display: "flex",
        justifyContent: "space-between",
        padding: theme.spacing(1, 0),
    },
    icon: {
        margin: theme.spacing(2, 0),
        marginRight: theme.spacing(2),
    },
    textField: {
        width: "100%",
    },
    column: {
        width: "48%"
    },
    matchButtonDiv: {
        display: "flex",
        justifyContent: "center",
    },
    matchButton: {
        width: "48%",
        fontSize: "24px",
    }
}));

const SimpleTable = ({headers, rows, ...props}) => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <MuiTable className={classes.table} size={"small"} aria-label={"a dense table"}>
                <TableHead>
                    <TableRow>
                        { headers.map((header, h)=><TableCell key={h}>{ header }</TableCell>) }
                    </TableRow>
                </TableHead>
                <TableBody>
                    { rows.map((row, i) => (<EnhancedTableRow columns={row} key={i}/>)) }
                </TableBody>
            </MuiTable>
        </TableContainer>
    );
};
SimpleTable.propTypes = {
    headers: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired,
};

const EnhancedTableRow = ({columns}) => {
    return (
        <TableRow>
            { columns.map((column,c)=> <TableCell key={c}>{ column }</TableCell>) }
        </TableRow>
    );
};
EnhancedTableRow.propTypes = {
    columns: PropTypes.array.isRequired,
};

export default SimpleTable;