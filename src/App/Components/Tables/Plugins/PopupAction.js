import {Plugin, Template, TemplateConnector, TemplatePlaceholder} from "@devexpress/dx-react-core";
import PropTypes from "prop-types";
import React from "react";

const popupAction = React.memo(({ PopupComponent }) => (
    <Plugin>
        <Template name={"popupAction"}>
            <TemplateConnector>
                { (
                    {
                        rows,
                        getRowId,
                        deletedRowIds,
                        ...resProps
                    },
                    {
                        cancelDeletedRows
                    },
                ) => {
                    const open = deletedRowIds.length > 0;
                    const targetRow = open ? rows.filter(row => getRowId(row) === deletedRowIds[0])[0] : {};
                    console.log(rows, getRowId, targetRow, open, resProps);

                    return (
                        <PopupComponent
                            open={open}
                            row={targetRow}
                            postAction={cancelDeletedRows}
                        />
                    );
                } }
            </TemplateConnector>
        </Template>
        <Template name={"root"}>
            <TemplatePlaceholder />
            <TemplatePlaceholder name={"popupAction"} />
        </Template>
    </Plugin>
));

popupAction.propTypes = {
    PopupComponent: PropTypes.element.isRequired
};

export default popupAction;