import DownloadRoundedIcon from "@mui/icons-material/DownloadRounded";
import Tooltip from "@mui/material/Tooltip";
import PropTypes from "prop-types";
import React from "react";

import Button from "../../../Components/Buttons/Button";

const ExportToggleButton = ({ onToggle, getMessage, buttonRef }) => (
    <Tooltip
        title={getMessage("showExportMenu")}
        placement={"bottom"}
        enterDelay={300}
    >
        <Button
            color={"primary"}
            variant={"outlined"}
            onClick={onToggle}
            ref={buttonRef}
            sx={{ml: 1}}
            style={{textTransform: "none"}}
            startIcon={<DownloadRoundedIcon />}
            size={"small"}
        >
            Export
        </Button>
    </Tooltip>
);

ExportToggleButton.propTypes = {
    onToggle: PropTypes.func.isRequired,
    getMessage: PropTypes.func.isRequired,
    buttonRef: PropTypes.any.isRequired
};

export default ExportToggleButton;