import moment from "moment/moment";

const isAfterDate = {
    name: "isAfterDate",
    validate: (earlierDate) => (value) => value ? moment(earlierDate) <= moment(value) : true
};

export default isAfterDate;