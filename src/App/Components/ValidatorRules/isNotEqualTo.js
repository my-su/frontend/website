const isNotEqualTo = {
    name: "isNotEqualTo",
    validate: (valueToBeCheckedAgainst) =>
        (value) => value !== valueToBeCheckedAgainst
};

export default isNotEqualTo;