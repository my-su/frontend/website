const isNoDuplicate = {
    name: "isNoDuplicate",
    validate: (value, list) => {
        // the list is transformed into a string, so we need to get the list
        // back first.
        list = list.split(",");
        // if value is a duplicate, then at least 2 items need to equal value
        // we are looking for no duplicates and know value is contained at least
        // once, so this reduces to checking whether the filtered list contains
        // just one item.
        return list.filter(item => item === value).length === 1;
    }
};

export default isNoDuplicate;