const hasNoSpaces = {
    name: "hasNoSpaces",
    validate: (value) => value ? !value.includes(" ") : true
};

export default hasNoSpaces;