import { isValid } from "iban";

const isIban = {
    name: "isIban",
    validate: (value) => value ? isValid(value) : true
};

export default isIban;