const isBiggerThanOrBlank = {
    name: "isBiggerThanOrBlank",
    validate: (lowerBound) => (value) => value ? lowerBound <= value : true
};

export default isBiggerThanOrBlank;