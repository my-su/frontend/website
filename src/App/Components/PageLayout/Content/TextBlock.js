import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";

import Block from "../../../Components/PageLayout/Content/Block";


const TextBlock = ({ title, body, children }) => {
    return (
        <Block>
            <Typography variant={"h4"}>{ title }</Typography>
            <Typography style={{marginTop: 20}} variant={"body1"}>
                { body }
            </Typography>
            { children }
        </Block>
    );
};

TextBlock.propTypes = {
    title: PropTypes.string.isRequired,
    body: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
    children: PropTypes.node
};

export default TextBlock;