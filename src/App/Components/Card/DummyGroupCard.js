import React from "react";

import GroupCard from "./GroupCard";
import GroupCardV2 from "./GroupCardV2";


const DummyGroupCard = () => {
    return <GroupCardV2
        title={"www.com"}
        description={"We are going to learn different kinds of species in nature that live together to form amazing environment."}
        photo={"https://image.freepik.com/free-photo/river-foggy-mountains-landscape_1204-511.jpg"}
        group_memberships={[
            {photo: "https://i.pravatar.cc/300?img=1"},
            {photo: "https://i.pravatar.cc/300?img=2"},
            {photo: "https://i.pravatar.cc/300?img=3"},
            {photo: "https://i.pravatar.cc/300?img=4"},
            {photo: "https://i.pravatar.cc/300?img=5"},
            {photo: "https://i.pravatar.cc/300?img=6"},
            {photo: "https://i.pravatar.cc/300?img=7"},
            {photo: "https://i.pravatar.cc/300?img=8"},
        ]}
    />;
};

export default DummyGroupCard;