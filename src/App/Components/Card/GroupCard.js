import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { useTheme } from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import PeopleCardFooter from "@mui-treasury/components/cardFooter/people";
import TextInfoContent from "@mui-treasury/components/content/textInfo";
import { useOverShadowStyles } from "@mui-treasury/styles/shadow/over";
import { useBlogTextInfoContentStyles } from "@mui-treasury/styles/textInfoContent/blog";
import cx from "clsx";
import PropTypes from "prop-types";
import React from "react";


const useStyles = makeStyles(theme => ({
    root: {
        margin: "auto",
        borderRadius: theme.spacing(2), // 16px
        transition: "0.3s",
        boxShadow: "0px 14px 80px rgba(34, 35, 58, 0.2)",
        position: "relative",
        minWidth: 304,
        maxWidth: 500,
        marginLeft: "auto",
        overflow: "initial",
        background: "#ffffff",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        paddingBottom: theme.spacing(2),
        [theme.breakpoints.up("md")]: {
            flexDirection: "row",
            paddingTop: theme.spacing(2),
        },
    },
    media: {
        width: "88%",
        marginLeft: 10,
        marginRight: 10,
        height: 0,
        paddingBottom: "48%",
        borderRadius: theme.spacing(2),
        backgroundColor: "#fff",
        position: "relative",
        transform: "translateY(8px)",
        paddingLeft: 10,
        paddingRight: 10,
        [theme.breakpoints.up("md")]: {
            width: "100%",
            marginTop: 0,
            transform: "translateX(8px)",
        },
        "&:after": {
            content: "\" \"",
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            borderRadius: theme.spacing(2), // 16
            opacity: 0.5,
        },
    },
    content: {
        padding: 24,
    },
    cta: {
        marginTop: 24,
        textTransform: "initial",
    },
}));

const GroupCard = (props) => {
    const styles = useStyles();
    const {
        button: buttonStyles,
        ...contentStyles
    } = useBlogTextInfoContentStyles();
    const shadowStyles = useOverShadowStyles();

    const { title, description, photo, group_memberships } = props;
    return (
        <Card className={cx(styles.root, shadowStyles.root)}>
            <CardMedia
                className={styles.media}
                image={photo}
            />
            <CardContent>
                <TextInfoContent
                    classes={contentStyles}
                    heading={title}
                    body={description}
                />
                { /*<Button className={buttonStyles}>Read more</Button>*/ }
                <Box px={3} pb={3}>
                    <PeopleCardFooter
                        faces={group_memberships.map(membership=>membership.photo)}
                    />
                </Box>
            </CardContent>
        </Card>
    );
};

GroupCard.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    photo: PropTypes.string.isRequired,
    group_memberships: PropTypes.array.isRequired,
};

export default GroupCard;
