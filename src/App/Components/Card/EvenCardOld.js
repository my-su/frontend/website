import {CardActionArea} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";

import Sushi from "../../../img/Sushi-11-me.jpg";

const useStyles = makeStyles(theme => ({
    table: {
        marginTop: theme.spacing(2),
    },
    cardRoot2: {
        margin: "auto",
        width: "80%"
    },
    list: {
        margin: "auto",
        width: "80%"
    },
    cardRoot: {
        width: "80%",
        position: "relative"
    },
    cardActionArea: {
        display: "flex",
    },
    cardMedia: {
        width: "50%"
    },
    cardContent: {
        width: "50%"
    },
    dateDiv: {
        width: "50px",
        left: "25px",
        zIndex: 48,
        backgroundColor: "white",
        position: "absolute",
        textAlign: "center"
    }
}));


const EventCard = () => {
    const classes = useStyles();
    return (
        <Card className={classes.cardRoot}>
            <div className={classes.dateDiv}>
                <Typography variant={"subtitle1"}>Feb</Typography>
                <Typography variant={"h5"}>15</Typography>
            </div>
            <CardActionArea className={classes.cardActionArea}>
                <CardMedia
                    className={classes.cardMedia}
                    component={"img"}
                    image={Sushi}
                />
                <CardContent className={classes.cardContent}>
                    <Typography component={"div"} variant={"h5"}>
                        Live From Space
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default EventCard;