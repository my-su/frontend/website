import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import { useOverShadowStyles } from "@mui-treasury/styles/shadow/over";
import { useBlogTextInfoContentStyles } from "@mui-treasury/styles/textInfoContent/blog";
import cx from "clsx";
import PropTypes from "prop-types";
import React from "react";

import Image from "../../../img/default_photo.jpg";
import Info from "../../Forms/InfoForms/Info";


const useStyles = makeStyles(theme => ({
    root: {
        margin: "auto",
        borderRadius: theme.spacing(2), // 16px
        transition: "0.3s",
        boxShadow: "0px 14px 80px rgba(34, 35, 58, 0.2)",
        position: "relative",
        width: "100%",
        marginLeft: "auto",
        overflow: "initial",
        background: theme.palette.background.paper,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        paddingBottom: theme.spacing(2),
        [theme.breakpoints.up("md")]: {
            flexDirection: "row",
            paddingTop: theme.spacing(2),
        },
    },
    media: {
        minWidth: 100,
        maxWidth: 250,
        width: "88%",
        marginLeft: 10,
        marginRight: 10,
        height: 200,
        borderRadius: theme.spacing(2),
        backgroundColor: theme.palette.background.paper,
        position: "relative",
        transform: "translateY(8px)",
        paddingLeft: 10,
        paddingRight: 10,
        [theme.breakpoints.up("md")]: {
            width: "100%",
            marginTop: 0,
            transform: "translateX(8px)",
        },
        "&:after": {
            content: "\" \"",
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            borderRadius: theme.spacing(2), // 16
            opacity: 0.5,
        },
    },
    content: {
        padding: 24,
    },
    cta: {
        marginTop: 24,
        textTransform: "initial",
    },
}));

const ContactCard = (props) => {
    const styles = useStyles();
    const {
        button: buttonStyles,
        ...contentStyles
    } = useBlogTextInfoContentStyles();
    const shadowStyles = useOverShadowStyles();

    const { duty, description, photo, data } = props;
    return (
        <Card className={cx(styles.root, shadowStyles.root)}>
            <CardMedia
                className={styles.media}
                image={photo || Image}
            />
            <CardContent>
                <Typography variant={"h5"}>{ duty }</Typography>
                <Info
                    headerless={true}
                    data={data}
                />
                <Typography variant={"body1"}>{ description }</Typography>
                { /*<Button className={buttonStyles}>Read more</Button>*/ }
            </CardContent>
        </Card>
    );
};

ContactCard.propTypes = {
    duty: PropTypes.string.isRequired,
    description: PropTypes.string,
    photo: PropTypes.string,
    data: PropTypes.object,
};

export default ContactCard;
