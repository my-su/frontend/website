import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import { useOverShadowStyles } from "@mui-treasury/styles/shadow/over";
import cx from "clsx";
import PropTypes from "prop-types";
import React from "react";
import LinesEllipsis from "react-lines-ellipsis";
import { loremIpsum } from "react-lorem-ipsum";
import { useHistory } from "react-router-dom";

import Image from "../../../img/default_photo.jpg";
import Button from "../../Components/Buttons/Button";
import Info from "../../Forms/InfoForms/Info";


const useStyles = makeStyles(theme => ({
    root: {
        margin: "auto",
        borderRadius: theme.shape.borderRadius,
        transition: "0.3s",
        boxShadow: "0px 14px 80px rgba(34, 35, 58, 0.2)",
        position: "relative",
        minWidth: 304,
        maxWidth: 500,
        marginLeft: "auto",
        overflow: "initial",
        background: theme.palette.background.paper,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    media: {
        width: "88%",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        height: 0,  
        paddingBottom: "48%",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
        position: "relative",
        transform: "translateY(8px)",
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        "&:after": {
            content: "\" \"",
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            borderRadius: theme.shape.borderRadius,
            opacity: 0.5,
        },
    },
    buttonSeparator : {
        display: "flex",
        justifyContent: "space-between",
        marginTop: theme.spacing(2),
    },
    content: {
        padding: 24,
    },
    cta: {
        marginTop: 24,
        textTransform: "initial",
    },
}));

const AssociationCard = ({ name, description, photo, slug, onBecomeMemberClick, becomeMemberButton }) => {
    const styles = useStyles();
    let history = useHistory();
    const shadowStyles = useOverShadowStyles();

    const text = description || loremIpsum()[0];

    return (
        <Card className={cx(styles.root, shadowStyles.root)}>
            <CardMedia
                className={styles.media}
                image={photo || Image}
            />
            <CardContent style={{width: "100%"}}>
                <Typography variant={"h5"}>{ name }</Typography>
                <Info
                    headerless={true}
                    data={{}}
                />
                <LinesEllipsis
                    className={`${styles.body} MuiTypography-root MuiTypography-body1`}
                    component={"p"}
                    maxLine={5}
                    text={text}
                />
                <Divider/>
                <div className={styles.buttonSeparator}>
                    <Button variant={"contained"} color={"primary"} onClick={()=>history.push("/protected/associations/" + slug + "/info")}>Read more</Button>
                    { becomeMemberButton && <Button variant={"contained"} color={"primary"} onClick={onBecomeMemberClick}>Become Member</Button> }
                </div>
            </CardContent>
        </Card>
    );
};

AssociationCard.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    photo: PropTypes.string.isRequired,
    onBecomeMemberClick: PropTypes.func.isRequired,
    becomeMemberButton: PropTypes.bool.isRequired
};

export default AssociationCard;
