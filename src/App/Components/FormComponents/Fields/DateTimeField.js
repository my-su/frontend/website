import DateTimePicker from "@mui/lab/DateTimePicker";
import Grid from "@mui/material/Grid";
import MuiTextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import classNames from "clsx";
import moment from "moment";
import PropTypes from "prop-types";
import React from "react";
import {ValidatorComponent} from "react-form-validator-core";

import useFieldStyles, {useFieldStylesV2} from "../fieldStyles";


const useStyles = makeStyles(theme => ({
    picker: {
        marginRight: theme.spacing(2),
        "&:last-child": {
            marginRight: 0,
        },
        width: "100%",
    },
}));

const DateTimeField = ({name, value, onChange, apiFormat, size, ...remaining_props}) => {
    const classes = useStyles();

    const handleChange = (datetime_util_object) => {
        onChange(datetime_util_object === null ? null : datetime_util_object.format(apiFormat));
    };

    return (
        <DateTimePicker
            className={classes.picker}
            ampm={false}
            inputVariant={"outlined"}
            renderInput={(props) => (<MuiTextField
                sx={{ width: "100%" }}
                label={name[0].toUpperCase() + name.slice(1).replaceAll("_", " ")}
                size={size}
                {...props}
            />)}
            {...remaining_props}
            onChange={handleChange}
            value={value && moment(value)}
        />
    );
};

DateTimeField.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    apiFormat: PropTypes.string,
    format: PropTypes.string,
    size: PropTypes.string
};

DateTimeField.defaultProps = {
    apiFormat: "YYYY-MM-DD HH:mm",
    format: "YYYY-MM-DD HH:mm"
};


const DateTimeFieldV2 = (props) => {
    const classes = useStyles();

    return (
        <DateTimePicker
            inputVariant={"outlined"}
            className={classes.picker}
            ampm={false}
            format={"LLLL"}
            {...props}
        />
    );
};

class DateTimeValidator extends ValidatorComponent {
    renderValidatorComponent() {
        /* eslint-disable no-unused-vars */
        const {
            error,
            errorMessages,
            validators,
            requiredError,
            helperText,
            validatorListener,
            withRequiredValidator,
            containerProps,
            ...rest
        } = this.props;
        const { isValid } = this.state;
        return (
            <DateTimeField
                {...rest}
                error={!isValid || error}
                helperText={(!isValid && this.getErrorMessage()) || helperText}
            />
        );
    }
}
const DateTimeFieldV3 = (props) => {
    const classes = useFieldStyles();

    return (
        <DateTimeValidator
            className={classes.field}
            containerProps={{className: classes.field}}
            inputVariant={"outlined"}
            ampm={false}
            format={"LLLL"}
            {...props}
        />
    );
};


const DateTimeFieldV4 = ({ variant, label, labelXs, ...rest_props }) => {
    const fieldClasses = useFieldStylesV2();
    const classes = useStyles();

    if (variant === "separated") {
        return (
            <Grid container spacing={2} alignItems={"center"}>
                <Grid item xs={labelXs}>
                    <Typography variant={"body1"}>{ label }</Typography>
                </Grid>
                <Grid item xs={12 - labelXs}>
                    <DateTimeValidator
                        inputVariant={"outlined"}
                        size={"small"}
                        className={classNames(fieldClasses.field, classes.picker, fieldClasses.field)}
                        ampm={false}
                        format={"LLLL"}
                        containerProps={{className: fieldClasses.field}}
                        {...rest_props}
                    />
                </Grid>
            </Grid>
        );
    } else {
        return (
            <DateTimeValidator
                inputVariant={variant}
                label={label}
                className={classNames(classes.picker, fieldClasses.field)}
                ampm={false}
                format={"LLLL"}
                containerProps={{className: fieldClasses.field}}
                {...rest_props}
            />
        );
    }
};

DateTimeFieldV4.propTypes = {
    variant: PropTypes.string,
    label: PropTypes.string,
    labelXs: PropTypes.number,
};

DateTimeFieldV4.defaultProps = {
    variant: "separated",
    label: "",
    labelXs: 4,
};

export { DateTimeFieldV2, DateTimeFieldV3, DateTimeFieldV4 };
export default DateTimeField;