import MuiPhoneInput from "material-ui-phone-number";
import React from "react";
import { ValidatorComponent } from "react-form-validator-core";

import useFieldStyles from "../fieldStyles";


class PhoneNumberValidatorField extends ValidatorComponent {
    renderValidatorComponent() {
        /* eslint-disable no-unused-vars */
        const {
            error,
            errorMessages,
            validators,
            requiredError,
            helperText,
            validatorListener,
            withRequiredValidator,
            containerProps,
            ...rest
        } = this.props;
        const { isValid } = this.state;
        return (
            <MuiPhoneInput
                {...rest}
                error={!isValid || error}
                helperText={(!isValid && this.getErrorMessage()) || helperText}
            />
        );
    }
}

const PhoneNumberField = (props) => {
    const classes = useFieldStyles();
    return (
        <PhoneNumberValidatorField
            inputClass={classes.field}
            containerProps={{className: classes.field}}
            defaultCountry={"nl"}
            variant={"outlined"}
            {...props}
        />
    );
};

export default PhoneNumberField;