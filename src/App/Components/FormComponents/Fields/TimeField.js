import TimePicker from "@mui/lab/TimePicker";
import MuiTextField from "@mui/material/TextField";
import React from "react";
import {ValidatorComponent} from "react-form-validator-core";

import useFieldStyles from "../fieldStyles";

class TimeValidator extends ValidatorComponent {
    renderValidatorComponent() {
        /* eslint-disable no-unused-vars */
        const {
            error,
            errorMessages,
            validators,
            requiredError,
            helperText,
            validatorListener,
            withRequiredValidator,
            containerProps,
            size,
            ...rest
        } = this.props;
        const { isValid } = this.state;
        return (
            <TimePicker
                renderInput={(props) => (<MuiTextField
                    sx={{ width: "100%" }}
                    error={!isValid || error}
                    helperText={(!isValid && this.getErrorMessage()) || helperText}
                    size={size}
                    {...props}
                />)}
                {...rest}
            />
        );
    }
}
const TimeField = (props) => {
    const classes = useFieldStyles();

    return (
        <TimeValidator
            className={classes.field}
            containerProps={{className: classes.field}}
            inputVariant={"outlined"}
            ampm={false}
            format={"HH:mm"}
            {...props}
        />
    );
};

export default TimeField;