import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import React from "react";
import {ValidatorComponent} from "react-form-validator-core";

class AutocompleteValidatorField extends ValidatorComponent {
    renderValidatorComponent() {
        /* eslint-disable no-unused-vars */
        const {
            error,
            errorMessages,
            validators,
            requiredError,
            helperText,
            validatorListener,
            withRequiredValidator,
            containerProps,
            label,
            variant,
            ...rest
        } = this.props;
        const { isValid } = this.state;
        return (
            <Autocomplete
                style={{ width: "100%" }}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        error={!isValid || error}
                        helperText={(!isValid && this.getErrorMessage()) || helperText}
                        label={label}
                        variant={variant}
                        inputProps={{
                            ...params.inputProps,
                            autoComplete: "new-password", // disable autocomplete and autofill
                        }}
                    />
                )}
                {...rest}
            />
        );
    }
}

const AutocompleteField = () => {
    return (
        <AutocompleteValidatorField
            variant={"outlined"}
        />
    );
};

export default AutocompleteField;