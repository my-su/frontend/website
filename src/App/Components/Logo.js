import "./Logo.css";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import React from "react";


const Logo = (props) => {
    let { logo } = props;

    return (
        <div className={"userPicCropper"}>
            { logo
                ?
                <img src={logo} alt={"bad description"} className={"userPic"} />
                :
                <FontAwesomeIcon className={"fa-xs"} icon={"user"} size={"2x"}/>
            }
        </div>
    );
};

Logo.propTypes = {
    logo: PropTypes.string.isRequired
};

export default Logo;