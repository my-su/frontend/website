import React from "react";

import { APIContext } from "../../Contexts/API";

/**
 * Provides the useAPI() functionality to class components

 * @param Component
 * @returns {function(*): *}
 */
const withMember = Component => (
    props => (
        <APIContext.Consumer>
            { context => <Component apiContext={context} {...props} /> }
        </APIContext.Consumer>
    )
);

export default withMember;

