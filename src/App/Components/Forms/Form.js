import PropTypes from "prop-types";
import React from "react";
import { ValidatorForm } from "react-material-ui-form-validator";

import Button from "../Buttons/Button";
import Wrapper from "../FormComponents/Wrapper";


export const GenericInfoFormForm = ({ Form, formObject, handleFormObjectChange, onSubmit, onCancel, allowDelete, submitButtonText }) => {
    return (
        <ValidatorForm
            onSubmit={onSubmit}
            onError={errors => console.log(errors)}
        >
            <Form formObject={formObject} handleFormObjectChange={handleFormObjectChange}/>
            <Wrapper>
                <div>
                    <Button variant={"outlined"} color={"secondary"} disabled={!allowDelete}>
                        Delete
                    </Button>
                </div>
                <div>
                    <Button type={"submit"} variant={"contained"} color={"primary"}>{ submitButtonText }</Button>
                    &nbsp;
                    <Button variant={"contained"} onClick={onCancel}>Cancel</Button>
                </div>
            </Wrapper>
        </ValidatorForm>
    );
};

GenericInfoFormForm.propTypes = {
    Form: PropTypes.elementType.isRequired,
    formObject: PropTypes.object.isRequired,
    handleFormObjectChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    allowDelete: PropTypes.bool.isRequired,
    submitButtonText: PropTypes.string
};

GenericInfoFormForm.defaultProps = {
    submitButtonText: "save"
};

export default GenericInfoFormForm;