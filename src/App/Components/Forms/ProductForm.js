import AccessTimeIcon from "@mui/icons-material/AccessTime";
import CreateIcon from "@mui/icons-material/Create";
import EuroIcon from "@mui/icons-material/Euro";
import SwapHorizIcon from "@mui/icons-material/SwapHoriz";
import {MenuItem} from "@mui/material";
import {toast} from "material-react-toastify";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";

import {useAPI} from "../../Contexts/API";
import Button from "../Buttons/Button";
import FileField from "../FormComponents/Fields/FileField";
import NumberField from "../FormComponents/Fields/NumberField";
import SelectField from "../FormComponents/Fields/SelectField";
import TextField from "../FormComponents/Fields/TextField";
import FontAwesomeIconHolder from "../FormComponents/FontAwesomeIconHolder";
import IconHolder from "../FormComponents/IconHolder";
import Wrapper from "../FormComponents/Wrapper";


const ProductForm = ({ association, product: propProduct, onProductChange, update}) => {
    const API = useAPI();

    let [product, setProduct] = useState(propProduct);

    const resetAssociation = () => setProduct(propProduct);

    const handleProductChange = (field, value) => setProduct(prevState=>(
        {...prevState, [field]: value}
    ));

    const handleSubmit = () => {
        API.callv4({
            url: association.url,
            method: "PATCH",
            object: {
                [product.what_it_is_for+"_subject"]: product.subject,
                [product.what_it_is_for+"_message"]: product.message,
            },
            on_succes: (association_with_updated_product) => {
                onProductChange(association_with_updated_product);
            },
            on_failure: () => {
                toast.error("Update product failed");
            }
        });
    };

    return (
        <ValidatorForm
            onSubmit={handleSubmit}
            onError={errors => console.log(errors)}
        >
            <Wrapper>
                <IconHolder Icon={CreateIcon}/>
                <TextField
                    name={"name"}
                    value={product.name}
                    onChange={(event)=>handleProductChange("name",event.target.value)}
                />
            </Wrapper>
            <Wrapper>
                <FontAwesomeIconHolder icon={["fas", "image"]}/>
                <FileField
                    name={"thumbnail"}
                    label={"Thumbnail"}
                    value={product.thumbnail}
                    onChange={(event)=>handleProductChange("thumbnail",event.target.value)}
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={SwapHorizIcon}/>
                <SelectField
                    name={"type"}
                    value={product.type}
                    onChange={(event)=>handleProductChange("type",event.target.value)}
                >
                    <MenuItem value={"rentable"}>Rentable</MenuItem>
                    <MenuItem value={"purchasable"}>Purchasable</MenuItem>
                </SelectField>
            </Wrapper>
            { product.type === "rentable" &&
                <Wrapper>
                    <IconHolder Icon={AccessTimeIcon}/>
                    <NumberField
                        name={"rental_period"}
                        value={product.rental_period}
                        onChange={(event)=>handleProductChange("rental_period",event.target.value)}
                        InputProps={{
                            inputProps: {
                                step: 1, min: 0
                            }
                        }}
                    />
                </Wrapper>
            }
            <Wrapper>
                <IconHolder Icon={EuroIcon}/>
                <NumberField
                    name={"price"}
                    value={product.price}
                    onChange={(event)=>handleProductChange("price",event.target.value)}
                    InputProps={{
                        inputProps: {
                            step: 0.1, min: 0
                        }
                    }}
                />
            </Wrapper>

            <Wrapper>
                <div>
                    <Button variant={"outlined"} color={"secondary"}>
                        Remove from listing
                    </Button>
                </div>
                <div>
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Save</Button>
                    &nbsp;
                    <Button variant={"contained"} onClick={resetAssociation}>Cancel</Button>
                </div>
            </Wrapper>
        </ValidatorForm>
    );
};

ProductForm.propTypes = {
    association: PropTypes.object.isRequired,
    onProductChange: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
    product: PropTypes.shape({
        name: PropTypes.string,
        thumbnail: PropTypes.string,
        type: PropTypes.string,
        rental_period: PropTypes.number,
        price_fixed: PropTypes.bool,
        price: PropTypes.number,
    }).isRequired,
};

export default ProductForm;