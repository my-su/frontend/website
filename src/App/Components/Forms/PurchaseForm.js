import AccessTimeIcon from "@mui/icons-material/AccessTime";
import EuroIcon from "@mui/icons-material/Euro";
import PersonIcon from "@mui/icons-material/Person";
import TodayIcon from "@mui/icons-material/Today";
import {MenuItem} from "@mui/material";
import {toast} from "material-react-toastify";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";

import {useAPI} from "../../Contexts/API";
import Button from "../Buttons/Button";
import DateField from "../FormComponents/Fields/DateField";
import NumberField from "../FormComponents/Fields/NumberField";
import SelectField from "../FormComponents/Fields/SelectField";
import FontAwesomeIconHolder from "../FormComponents/FontAwesomeIconHolder";
import IconHolder from "../FormComponents/IconHolder";
import Wrapper from "../FormComponents/Wrapper";


const PurchaseForm = ({ association, purchase: propPurchase, onPurchaseChange}) => {
    const API = useAPI();

    let [purchase, setPurchase] = useState(propPurchase);

    const resetAssociation = () => setPurchase(propPurchase);

    const handlePurchaseChange = (field, value) => setPurchase(prevState=>(
        {...prevState, [field]: value}
    ));

    const handleSubmit = () => {
        API.callv4({
            url: association.url,
            method: "PATCH",
            object: {
                [purchase.what_it_is_for+"_subject"]: purchase.subject,
                [purchase.what_it_is_for+"_message"]: purchase.message,
            },
            on_succes: (association_with_updated_purchase) => {
                onPurchaseChange(association_with_updated_purchase);
            },
            on_failure: () => {
                toast.error("Update purchase failed");
            }
        });
    };

    return (
        <ValidatorForm
            onSubmit={handleSubmit}
            onError={errors => console.log(errors)}
        >
            <Wrapper>
                <IconHolder Icon={PersonIcon}/>
                <SelectField
                    name={"member"}
                    value={purchase.profile}
                    onChange={(event)=>handlePurchaseChange("profile",event.target.value)}
                >
                    <MenuItem value={"1234"}>John Doe</MenuItem>
                </SelectField>
            </Wrapper>
            <Wrapper>
                <FontAwesomeIconHolder icon={["fas", "cookie-bite"]}/>
                <SelectField
                    name={"product"}
                    value={purchase.product}
                    onChange={(event)=>handlePurchaseChange("product",event.target.value)}
                >
                    <MenuItem value={"misc"}>misc</MenuItem>
                    <MenuItem value={"misc"}>cookie</MenuItem>
                    <MenuItem value={"misc"}>pie</MenuItem>
                    <MenuItem value={"misc"}>chocolate milk</MenuItem>
                </SelectField>
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={AccessTimeIcon}/>
                <NumberField
                    name={"amount"}
                    value={purchase.amount}
                    onChange={(event)=>handlePurchaseChange("amount",event.target.value)}
                    InputProps={{
                        inputProps: {
                            step: 1, min: 0
                        }
                    }}
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={EuroIcon}/>
                <NumberField
                    name={"price"}
                    value={purchase.price}
                    onChange={(event)=>handlePurchaseChange("price",event.target.value)}
                    InputProps={{
                        inputProps: {
                            step: 0.1, min: 0
                        }
                    }}
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={TodayIcon}/>
                <DateField
                    name={"purchase date"}
                    value={purchase.purchase_date}
                    onChange={(date)=>handlePurchaseChange("purchase_date", date)}
                />
            </Wrapper>
            <Wrapper>
                <div>
                    <Button variant={"outlined"} color={"secondary"}>
                        Delete
                    </Button>
                </div>
                <div>
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Save</Button>
                    &nbsp;
                    <Button variant={"contained"} onClick={resetAssociation}>Cancel</Button>
                </div>
            </Wrapper>
        </ValidatorForm>
    );
};

PurchaseForm.propTypes = {
    association: PropTypes.object.isRequired,
    onPurchaseChange: PropTypes.func.isRequired,
    purchase: PropTypes.shape({
        profile: PropTypes.string,
        product: PropTypes.string,
        amount: PropTypes.string,
        price: PropTypes.string,
        rental_period: PropTypes.number,
        purchase_date: PropTypes.string,
    }).isRequired,
};

export default PurchaseForm;