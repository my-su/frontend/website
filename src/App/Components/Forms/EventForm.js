import BallotIcon from "@mui/icons-material/Ballot";
import CalendarToday from "@mui/icons-material/CalendarToday";
import Create from "@mui/icons-material/Create";
import LocationOn from "@mui/icons-material/LocationOn";
import Notes from "@mui/icons-material/Notes";
import FormControlLabel from "@mui/material/FormControlLabel";
import MenuItem from "@mui/material/MenuItem";
import Switch from "@mui/material/Switch";
import {toast} from "material-react-toastify";
import PropTypes from "prop-types";
import React from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import {useHistory} from "react-router-dom";

import {useAPI} from "../../Contexts/API";
import {useGetEventTypesByAssociationQuery} from "../../Store/services/MySU/eventTypes";
import Button from "../Buttons/Button";
import DateTimeField from "../FormComponents/Fields/DateTimeField";
import NumberField from "../FormComponents/Fields/NumberField";
import SelectField from "../FormComponents/Fields/SelectField";
import TextField from "../FormComponents/Fields/TextField";
import IconHolder from "../FormComponents/IconHolder";
import Wrapper from "../FormComponents/Wrapper";


export const EventForm = ({ association, event, handleEventChange, update, onSucces, onCancel}) => {
    const API = useAPI();
    const history = useHistory();

    const { data: eventTypes } = useGetEventTypesByAssociationQuery(association.slug);

    const handleSubmit = () => {
        // make a copy of the event so that the null key operation don't accidentally change the event prop
        let toBeSentEvent = {...event, permissions: []};
        // Drop all field with value null
        let null_keys = [];
        Object.entries(event).forEach(([key, value])=> {
            if (value === null) {
                null_keys.push(key);
            }
        });
        null_keys.forEach(null_key=> delete toBeSentEvent[null_key]);

        if (update) {
            patchEvent(toBeSentEvent);
        } else {
            postEvent(toBeSentEvent);
        }
    };

    const postEvent = (event) => {
        return API.callv4({
            url: "/events",
            method: "POST",
            object: event,
            on_succes: (data) => {
                onSucces(data);
            },
            on_failure: (data) => {
                toast.error("Save failed");
            }
        });
    };

    const patchEvent = (event) => {
        return API.callv4({
            url: "/events/" + event.slug,
            method: "PATCH",
            object: event,
            on_succes: (data) => {
                onSucces(data);
            },
            on_failure: () => {
                toast.error("Save failed");
            },
        });
    };

    const deleteEvent = () => {
        return API.callv4({
            url: "/events/" + event.slug,
            method: "DELETE",
            on_succes: (data) => {
                history.replace(".");
            },
            on_failure: () => {
                toast.error("Delete failed");
            },
        });
    };

    return (
        <ValidatorForm
            onSubmit={handleSubmit}
            onError={errors => console.log(errors)}
        >
            <Wrapper>
                <IconHolder Icon={Create}/>
                <TextField
                    name={"name"}
                    value={event.name}
                    onChange={(event) => handleEventChange("name", event.target.value)}
                    validators={["minStringLength:4"]}
                    errorMessages={["Name must have at least 4 characters"]}
                    required
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={CalendarToday}/>
                <DateTimeField
                    name={"Start date"}
                    value={event.start_date}
                    onChange={date => handleEventChange("start_date", date)}
                />
                <DateTimeField
                    name={"End date"}
                    value={event.end_date}
                    onChange={date => handleEventChange("end_date", date)}
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={Notes}/>
                <TextField
                    name={"description"}
                    value={event.description}
                    onChange={(event) => handleEventChange("description", event.target.value)}
                    multiline
                    rows={"6"}
                />
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={BallotIcon}/>
                <SelectField
                    name={"type"}
                    value={event.type}
                    validators={["required"]}
                    errorMessages={["A type has to be set"]}
                    onChange={(event) => handleEventChange("type", event.target.value)}
                    required
                >
                    { eventTypes && eventTypes.map(event_type => (
                        <MenuItem key={event_type.slug} value={event_type.url}>
                            { event_type.type }
                        </MenuItem>
                    )) }
                </SelectField>
            </Wrapper>
            <Wrapper>
                <IconHolder Icon={LocationOn}/>
                <TextField
                    name={"location"}
                    value={event.location}
                    onChange={(event) => handleEventChange("location", event.target.value)}
                />
            </Wrapper>
            <FormControlLabel
                control={<Switch color={"primary"} />}
                label={"Public"}
                labelPlacement={"start"}
                checked={event.public}
                onChange={(event) => handleEventChange("public", event.target.checked)}
            />
            <FormControlLabel
                control={<Switch color={"primary"} />}
                label={"Enrollable"}
                labelPlacement={"start"}
                checked={event.enrollable}
                onChange={(event) => handleEventChange("enrollable", event.target.checked)}
                disabled={event.public}
            />
            <FormControlLabel
                control={<Switch color={"primary"} />}
                label={"Unenrollable"}
                labelPlacement={"start"}
                checked={event.unenrollable}
                onChange={(event) => handleEventChange("unenrollable", event.target.checked)}
                disabled={!event.enrollable}
            />
            { event.enrollable &&
            <Wrapper>
                <IconHolder Icon={CalendarToday}/>
                <DateTimeField
                    name={"Enrollable from"}
                    value={event.enrollable_from}
                    onChange={(date) => handleEventChange("enrollable_from", date)}
                    // onChange={(datetime_util_object) => handleEventChange("enrollable_from", datetime_util_object._d)}
                />
                <DateTimeField
                    name={"Enrollable until"}
                    value={event.enrollable_until}
                    onChange={(date) => handleEventChange("enrollable_until", date)}
                />
            </Wrapper>
            }
            { event.unenrollable &&
            <Wrapper>
                <IconHolder Icon={CalendarToday}/>
                <DateTimeField
                    name={"Unenrollable until"}
                    value={event.unenrollable_until}
                    onChange={(date) => handleEventChange("unenrollable_until", date)}
                />
            </Wrapper>
            }
            { event.enrollable &&
            <Wrapper>
                <IconHolder Icon={LocationOn}/>
                <NumberField
                    name={"Maximum number of enrollments"}
                    value={event.max_number_of_enrollments}
                    onChange={(event) => handleEventChange("max_number_of_enrollments", event.target.value)}
                    InputProps={{
                        inputProps: {
                            step: 1, min: 0
                        }
                    }}
                    helperText={"A maximum of `0` means no limit to the number of enrollments"}
                    required
                />
            </Wrapper>
            }

            <Wrapper>
                <div>
                    <Button variant={"outlined"} color={"secondary"} onClick={deleteEvent}>
                        Delete
                    </Button>
                </div>
                <div>
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Save</Button>
                    &nbsp;
                    <Button variant={"contained"} onClick={onCancel}>Cancel</Button>
                </div>
            </Wrapper>
        </ValidatorForm>
    );
};

EventForm.propTypes = {
    event: PropTypes.object.isRequired,
    handleEventChange: PropTypes.func.isRequired,
    update: PropTypes.bool.isRequired,
    onSucces: PropTypes.func,
    association: PropTypes.object
};

EventForm.defaultProps = {
    onSucces: ()=> {}
};