import React, { Component } from "react";

import Button from "../../Components/Buttons/Button";

export class Osiris extends Component {
    postProfile(){
        let profile = {
            "given_name": "",
            "initials": "",
            "surname": "",
            "gender": "",
            "date_of_birth": null,
            "phone_number": "",
            "email": "",
            "address": "",
            "zip_code": "",
            "city": "",
            "country": "",
            "is_osiris": true,
            "profilename": "current"
        };
        return fetch(process.env.REACT_APP_API_URL + "/profiles", {
            method: "POST",
            redirect: "follow",
            headers: {
                "Authorization": localStorage.getItem("SUN_api_token"),
                "Content-Type": "application/json"
            },
            body:JSON.stringify(profile)
        }).then((resp) => {
            console.log(resp);
        });
    }

    setupOsirisSync = () => {
        this.postProfile();
        return window.location.href = "https://webapps.utwente.nl/permissionportal/request/?service=1&returnto=https://sun.snt.utwente.nl";
    };

    render() {
        return (
            <x-fragment>
                <a color={"primary"} className={"btn btn-secondary"} href={"https://webapps.utwente.nl/permissionportal/request/?service=1&returnto=https://sun.snt.utwente.nl"}>Click here import from Osiris</a>
                <Button onClick={() => this.setupOsirisSync()} color={"primary"}>Click here sync with Osiris</Button>
            </x-fragment>
        );
    }
}
