import PropTypes from "prop-types";
import React from "react";

import Button from "./Button";


const InfoFormStateButton = ({ formState, formStateToggle }) => {
    return (
        <Button color={"primary"} variant={"contained"} onClick={formStateToggle}>
            { formState === "form" ? "View" : "Edit" }
        </Button>
    );
};

InfoFormStateButton.propTypes = {
    formState: PropTypes.string.isRequired,
    formStateToggle: PropTypes.func.isRequired
};

export default InfoFormStateButton;