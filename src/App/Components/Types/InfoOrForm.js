const InfoOrForm = (props, propName, componentName) => {
    if (props[propName] === "info" || props[propName] === "form") {
        return new Error(
            "Invalid prop " + propName+" supplied to " + componentName + ". " +
            "Must be either 'info' or 'form', but " + props[propName] + " given."
        );
    }
};

export default InfoOrForm;