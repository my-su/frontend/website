import PropTypes from "prop-types";

const AssociationType = PropTypes.object.isRequired;

export default AssociationType;