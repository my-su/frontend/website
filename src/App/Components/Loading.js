// Taken from https://github.com/DevExpress/devextreme-reactive/issues/3016
import CircularProgress from "@mui/material/CircularProgress";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";

const useStyles = makeStyles({
    loadingShading: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        background: "rgba(255, 255, 255, .3)"
    },
    loadingIcon: {
        position: "absolute",
        fontSize: "20px",
        top: "calc(45% - 10px)",
        left: "calc(50% - 10px)"
    }
});

const Loading = () => {
    const classes = useStyles();
    return (
        <div className={classes.loadingShading}>
            <CircularProgress className={classes.loadingIcon} />
        </div>
    );
};

export default Loading;
