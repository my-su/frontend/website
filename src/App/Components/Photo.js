import {Typography} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React from "react";

import Block from "./PageLayout/Content/Block";

const useStyles = makeStyles(theme => ({
    image: {
        maxWidth: "100%"
    }
}));


const Photo = (props) => {
    const classes = useStyles();

    const { photo } = props;

    return (
        <Block className={"mt-20"}>
            <Typography variant={"h5"}>Photo</Typography>
            <hr className={"box-title-separator"}/>
            { photo !== null && <img className={classes.image} src={photo} alt={"bad description"} /> }
        </Block>
    );
};

Photo.propTypes = {
    photo: PropTypes.string.isRequired
};

export default Photo;