import {
    IntegratedSummary,
    SummaryState,
} from "@devexpress/dx-react-grid";
import {
    Grid,
    Table,
    TableHeaderRow,
    TableSummaryRow
} from "@devexpress/dx-react-grid-material-ui";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";

import {useGetDebtEntriesDetailQuery} from "../../../../Store/services/MySU/debtEntries";

const headers = [
    {name: "reason", title: "Reason"},
    {name: "date_issued", title: "Date Issued"},
    {name: "price", title: "Price"},
];
const totalItems = [
    { columnName: "price", type: "max" },
    { columnName: "price", type: "sum" },
];
const getRowId = row => row.slug;

const InvoiceDetail = ({ slug }) => {
    const { invoices } = useGetDebtEntriesDetailQuery(slug, {
        selectFromResult: ({ data }) => ({
            invoices: data?.invoices,
        }),
    });
    const rows = invoices?.map(invoice=>({...invoice, price: parseFloat(invoice.price)}));
    // const rows = invoices?.map(invoice=>({...invoice, price: "€ " + invoice.price.toLocaleString("en-GB", { style: "currency", currency: "EUR" }) }));
    if (!rows?.length > 0) {
        return null;
    }
    return (
        <>
            <Typography variant={"h5"}>Individual payments</Typography>
            <Grid
                rows={rows}
                columns={headers}
                getRowId={getRowId}
            >
                <SummaryState
                    totalItems={totalItems}
                />
                <IntegratedSummary />
                <Table />
                <TableHeaderRow />
                <TableSummaryRow />
            </Grid>
        </>
    );
};

InvoiceDetail.propTypes = {
    slug: PropTypes.string.isRequired,
};

export default InvoiceDetail;