import Typography from "@mui/material/Typography";
import moment from "moment";
import PropTypes from "prop-types";
import React, {useState} from "react";
import { useParams } from "react-router-dom";

import {BackButton} from "../../../../Components/BackButton";
import Wrapper from "../../../../Components/FormComponents/Wrapper";
import Block from "../../../../Components/PageLayout/Content/Block";
import ExtremeTable from "../../../../Components/Tables/ExtremeTable";
import DebtEntryPopupForm from "../../../../Forms/PopupForms/DebtEntry/DebtEntryPopupForm";
import { useGetDebtCollectionsByAssociationQuery } from "../../../../Store/services/MySU/debtCollections";
import {
    useGetDebtEntriesByDebtCollectionQuery,
    usePatchDebtEntryMutation
} from "../../../../Store/services/MySU/debtEntries";
import EventDetail from "./EventDetail";
import InvoiceDetail from "./InvoiceDetail";
import MemberTypeDetail from "./MemberTypeDetail";
import ProductDetail from "./ProductDetail";

const headers = [
    {name: "name", title: "Who"},
    {name: "total_price", title: "Amount"},
    {name: "mandate_id", title: "Mandate ID"},
    {name: "bank_account_name", title: "Bank account name"},
    {name: "signature_date", title: "Signature date"},
    {name: "iban", title: "IBAN"},
    {name: "bic", title: "BIC"},
    {name: "email", title: "Email"},
    {name: "cancelled", title: "Cancelled"},
    {name: "paid_by", title: "Payment by"},
];
const booleanColumns = ["cancelled"];
const currencyColumns = ["total_price"];
const dateColumns = ["debt_collection_mandate_signature_date"];

const defaultHiddenColumnNames = ["mandate_id", "bank_account_name", "signature_date", "bic"];

const RowDetail = ({ row }) => {
    return (
        <div>
            <EventDetail slug={row.slug}/>
            <ProductDetail slug={row.slug}/>
            <InvoiceDetail slug={row.slug}/>
            <MemberTypeDetail slug={row.slug}/>
        </div>
    );
};

const DebtEntries = ({ association }) => {
    const { slug } = useParams();
    const [selection, setSelection] = useState([]);

    const { debtCollection } = useGetDebtCollectionsByAssociationQuery(association.slug, {
        selectFromResult: ({ data }) => ({
            debtCollection: data?.find((debtCollection) => debtCollection.slug === slug),
        }),
    });
    const { data: rows } = useGetDebtEntriesByDebtCollectionQuery(slug);
    const [ patchDebtEntry ] = usePatchDebtEntryMutation();

    const onEdit = (differences, original_row, edited_row, edited_row_index) => {
        patchDebtEntry({
            slug: original_row.slug,
            debtCollectionSlug: slug,
            associationSlug: association.slug,
            cancelled: edited_row.cancelled
        });
    };
    return (
        <>
            <BackButton/>
            <Block>
                <Wrapper>
                    <Typography variant={"h5"}>
                        Debt Collection { "of " + moment(debtCollection?.debt_collection_date).format("LLLL") }
                    </Typography>
                </Wrapper>
                <hr className={"box-title-separator"}/>
                <ExtremeTable
                    headers={headers}
                    rows={rows || []}
                    booleanColumns={booleanColumns}
                    currencyColumns={currencyColumns}
                    dateColumns={dateColumns}
                    defaultHiddenColumnNames={defaultHiddenColumnNames}
                    showExporter={true}
                    showGrouping={false}
                    showSelect={false}
                    showEditing={true}
                    selection={{selection: selection, setSelection: setSelection}}
                    allowDelete={false}
                    showDetail={true}
                    RowDetail={RowDetail}
                    editThroughPopup={true}
                    onEdit={onEdit}
                    Popup={DebtEntryPopupForm}
                />
            </Block>
        </>
    );
};

DebtEntries.propTypes = {
    association: PropTypes.object.isRequired
};

export { RowDetail };
export default DebtEntries;