import {
    Grid,
    Table,
    TableHeaderRow,
} from "@devexpress/dx-react-grid-material-ui";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";

import {useGetDebtEntriesDetailQuery} from "../../../../Store/services/MySU/debtEntries";

const headers = [
    {name: "type", title: "Type"},
    {name: "fee", title: "Fee"},
];
const getRowId = row => row.slug;

const MemberTypeDetail = ({ slug }) => {
    const { membershipFees } = useGetDebtEntriesDetailQuery(slug, {
        selectFromResult: ({ data }) => ({
            membershipFees: data?.membership_fees,
        }),
    });
    const rows = membershipFees;
    if (!rows?.length > 0) {
        return null;
    }
    return (
        <>
            <Typography variant={"h5"}>Membership fee</Typography>
            <Grid
                rows={rows}
                columns={headers}
                getRowId={getRowId}
            >
                <Table />
                <TableHeaderRow />
            </Grid>
        </>
    );
};

MemberTypeDetail.propTypes = {
    slug: PropTypes.string.isRequired,
};

export default MemberTypeDetail;