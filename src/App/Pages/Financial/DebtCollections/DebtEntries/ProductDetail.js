import {
    IntegratedSummary,
    SummaryState,
} from "@devexpress/dx-react-grid";
import {
    Grid,
    Table,
    TableHeaderRow,
    TableSummaryRow
} from "@devexpress/dx-react-grid-material-ui";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";

import {useGetDebtEntriesDetailQuery} from "../../../../Store/services/MySU/debtEntries";

const headers = [
    {name: "product", title: "Product"},
    {name: "cost", title: "Unit Cost"},
    {name: "number", title: "Number"},
    {name: "amount", title: "Amount"},
];
const totalItems = [
    { columnName: "amount", type: "max" },
    { columnName: "amount", type: "sum" },
    { columnName: "number", type: "sum" },
];
const getRowId = row => row.slug;

const ProductDetail = ({ slug }) => {
    const { products } = useGetDebtEntriesDetailQuery(slug, {
        selectFromResult: ({ data }) => ({
            products: data?.products,
        }),
    });
    const rows = products?.map(product=>({...product, amount: product.cost * product.number }));
    if (!rows?.length > 0) {
        return null;
    }
    return (
        <>
            <Typography variant={"h5"}>Products</Typography>
            <Grid
                rows={rows}
                columns={headers}
                getRowId={getRowId}
            >
                <SummaryState
                    totalItems={totalItems}
                />
                <IntegratedSummary />
                <Table />
                <TableHeaderRow />
                <TableSummaryRow />
            </Grid>
        </>
    );
};

ProductDetail.propTypes = {
    slug: PropTypes.string.isRequired,
};

export default ProductDetail;