import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React from "react";
import {useParams} from "react-router-dom";

import { BackButton } from "../../Components/BackButton";
import Block from "../../Components/PageLayout/Content/Block";
import ProfileInfo from "../../Forms/InfoForms/Profile/ProfileInfo";


const MergeProfilesView = ({ profiles }) => {
    const { profileSlug } = useParams();
    const profile = profiles.find(profile=>profile.slug === profileSlug);

    return(
        <>
            <BackButton container={true}/>
            <Container>
                <Block className={"mt-20"}>
                    <Typography variant={"h5"}>{ profile.profilename || "Profile" }</Typography>
                    <hr className={"box-title-separator"}/>
                    <ProfileInfo
                        profile={profile}
                    />
                </Block>
            </Container>
        </>
    );
};

MergeProfilesView.propTypes = {
    profiles: PropTypes.array.isRequired
};

export default MergeProfilesView;
