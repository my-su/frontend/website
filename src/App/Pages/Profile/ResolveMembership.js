import Container  from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React, {useState} from "react";
import { ValidatorForm } from "react-material-ui-form-validator";

import Button from "../../Components/Buttons/Button";
import CheckboxField from "../../Components/FormComponents/Fields/CheckboxField";
import Block from "../../Components/PageLayout/Content/Block";
import {useResolveMembershipsMutation} from "../../Store/services/MySU/user";

/**
 * Component with form for resolving "Claimed" memberships
 *
 * Memberships can have multiple status's. Claimed memberships must be resolved.
 * This component takes the memberships that are claimed and creates a checkbox
 * for each membership. Depending on whether it is checked it will be mapped to
 * "Accepted" or "Disputed". "Accepted" means that both the user and the 
 * association think the user is a member, whereas with "Disputed" the 
 * association says the user is a member but the user says otherwise.
 * 
 * @param claimed_memberships List of memberships with status="Claimed"
 * @param onResolve Function called after the api has confirmed a valid submit
 * @returns {JSX.Element}
 * @constructor
 */
const ResolveMembership = ({ claimed_memberships }) => {
    const [ resolveMemberships ] = useResolveMembershipsMutation();

    // This creates a list ['slug1', 'slug2',...] with each slug corresponding
    // to the slug of a "status=Claimed" membership
    const claimed_membership_slugs = claimed_memberships.map(membership => membership.slug);
    // We need to save the slugs in a state to be able to use it in a form.
    // The number of slugs is not always the same. We could create a state for each.
    // However having a dynamic number of state variables leads to all sorts of problems.
    // Hence we do some manipulation to save all in a single state variable.
    // To save the slugs in a single state variable we need to map the slugs to a single object
    // This changes ['slug1', 'slug2',...] to {'slug1': false, 'slug2': false,...}
    const init_for_checkboxes = claimed_membership_slugs.reduce(
        (options, option) => ({
            ...options,
            [option]: false
        }),
        {}
    );

    const [checkboxes, setCheckboxes] = useState(init_for_checkboxes);

    const handleCheckboxChange = (field, value) => {
        setCheckboxes(prevState=>({
            ...prevState,
            [field]: value
        }));
    };

    const handleSubmit = () => {
        // The API endpoint expects an object like {'membership_dict_list':{slug: action, slug: action, ...}}
        // with action being either "Accepted" or "Disputed"
        // Fields when checked represent "Accepted", and when unchecked "Disputed"
        // This transforms {'slug1': bool, 'slug2': bool,...} to [['slug1', bool], ['slug2', bool],...] to {'slug1': action, 'slug2': action,...}
        const resolveDict = Object.entries(checkboxes).reduce(
            (Y,[slug,checked])=>({
                ...Y,
                [slug]: checked ? "Accepted" : "Disputed"}
            ),
            {}
        );
        resolveMemberships(resolveDict);
    };

    return (
        <Container>
            <Block>
                <Typography variant={"h5"}>Confirm Memberships</Typography>
                <hr className={"box-title-separator"}/>
                <Typography variant={"body1"}>New associations have joined MySU. According to some of them you are a member of their association. Please check the boxes of the association you are a member of, and submit to confirm that you are indeed a member.</Typography>
                <ValidatorForm
                    onSubmit={handleSubmit}
                    onError={errors => console.error(errors)}
                >
                    { claimed_memberships.map((membership, m)=>(
                        <CheckboxField
                            key={m}
                            label={membership.association.name}
                            value={checkboxes[membership.slug]}
                            checked={checkboxes[membership.slug]}
                            onChange={event=>handleCheckboxChange(membership.slug, event.target.checked)}
                            labelPlacement={"end"}
                        />
                    )) }
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Submit</Button>
                </ValidatorForm>
            </Block>
        </Container>
    );
};

ResolveMembership.propTypes = {
    claimed_memberships: PropTypes.array.isRequired,
};

export default ResolveMembership;
