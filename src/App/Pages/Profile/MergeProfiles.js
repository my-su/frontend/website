import Container from "@mui/material/Container";
import MenuItem from "@mui/material/MenuItem";
import { useTheme } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React, {useState} from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import {NavLink, useRouteMatch} from "react-router-dom";

import Button from "../../Components/Buttons/Button";
import SelectField from "../../Components/FormComponents/Fields/SelectField";
import Wrapper from "../../Components/FormComponents/Wrapper";
import { Profile } from "../../Components/InfoForms/Profile";
import Block from "../../Components/PageLayout/Content/Block";
import {useMergeProfilesMutation} from "../../Store/services/MySU/user";


const useStyles = makeStyles(theme=>({
    buttons: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    }
}));

const MergeProfiles = ({ profiles }) => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const { url } = useRouteMatch();

    const [formType, setFormType] = useState("");
    const [profileSelector, setProfileSelector] = useState(null);

    const [ mergeProfiles ] = useMergeProfilesMutation();

    const handleSubmitSelector = () => {
        handleMerge(profileSelector);
    };

    const onProfileCreate = (profile) => {
        handleMerge({slug: profile.slug});
    };

    const handleMerge = (profileSlug) => {
        mergeProfiles({slug: profileSlug});
    };

    return (
        <Container>
            <Block>
                <Typography variant={"h5"}>Resolve Profiles</Typography>
                <hr className={"box-title-separator"}/>
                <Typography variant={"body1"}>
                    Multiple associations have a different profile of you.

                    To make full use of the benefits of MySU, you need to reduce that to one.

                    You can inspect the known profiles below by clicking on them.
                </Typography>
                <br/>
                { profiles.map((profile, p)=>{
                    return (
                        <React.Fragment key={p}>
                            <NavLink to={url + "/view/"+profile.slug}>
                                { profile.profilename || profile.slug }
                            </NavLink>
                            <br/>
                        </React.Fragment>
                    );
                }) }
                <Typography variant={"body1"}>Do you wish to use any of the above profiles or create a fresh one?</Typography>
                <Wrapper>
                    <Button className={classes.buttons} color={"primary"} variant={"contained"}
                        onClick={()=>{setFormType("selector");}}
                    >
                        Use existing
                    </Button>
                    <Button className={classes.buttons} color={"primary"} variant={"contained"}
                        onClick={()=>{setFormType("create");}}
                    >
                        Create new
                    </Button>
                </Wrapper>
            </Block>

            { formType === "selector" &&
            <Block>
                <Typography variant={"h4"}>Profile</Typography>
                <hr className={"box-title-separator"}/>
                <Typography variant={"body1"}>Which one do you want to use?</Typography>
                <br/>
                <ValidatorForm
                    onSubmit={handleSubmitSelector}
                    onError={errors => console.log(errors)}
                >
                    <SelectField
                        name={"Profile"}
                        value={profileSelector}
                        onChange={event => setProfileSelector(event.target.value)}
                    >
                        { profiles.map((profile,c)=>(
                            <MenuItem key={c} value={profile.slug}>{ profile.profilename }</MenuItem>
                        )) }
                    </SelectField>
                    <Wrapper>
                        <div/>
                        <Button color={"primary"} variant={"contained"} type={"submit"}>Select</Button>
                    </Wrapper>
                </ValidatorForm>
            </Block>
            }

            { formType === "create" &&
            <Block>
                <Typography variant={"h4"}>Profile</Typography>
                <hr className={"box-title-separator"}/>
                <Profile
                    infoOrForm={"form"}
                    profile={{}}
                    container={false}
                    onSuccess={onProfileCreate}
                />
            </Block>
            }
        </Container>
    );
};

MergeProfiles.propTypes = {
    profiles: PropTypes.array.isRequired,
};

export default MergeProfiles;
