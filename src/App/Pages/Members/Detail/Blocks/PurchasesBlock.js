import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import React, {useMemo} from "react";

import Wrapper from "../../../../Components/FormComponents/Wrapper";
import Block from "../../../../Components/PageLayout/Content/Block";
import Divider from "../../../../Components/PageLayout/Content/Divider";
import ExtremeTable from "../../../../Components/Tables/ExtremeTable";
import {useGetPurchasesByAssociationAndProfileQuery} from "../../../../Store/services/MySU/purchases";

const purchases_headers = [
    {name: "product", title: "Product"},
    {name: "price", title: "Price"},
    {name: "amount", title: "Amount"},
    {name: "total", title: "Total"},
    {name: "when", title: "When"}
];

const purchases = [];

const PurchasesBlock = ({ profileSlug, associationSlug }) => {
    // TODO: uncomment once purchases are implemented in the backend
    // const { data: purchases } = useGetPurchasesByAssociationAndProfileQuery({ profileSlug: slug, associationSlug: association.slug});
    const purchases_rows = useMemo(()=>purchases?.map(purchase=>({
        slug: purchase.slug,
        product: purchase.product,
        price: purchase.price,
        amount: purchase.amount,
        total: purchase.total,
        purchase_date: purchase.purchase_date,
    })),[purchases]);

    return (
        <Block>
            <ExtremeTable
                rows={purchases_rows || []}
                headers={purchases_headers}
                showSelect={false}
                showGrouping={false}
                showColumnChooser={false}
                currencyColumns={["price", "total"]}
                title={"Purchases"}
            />
        </Block>
    );
};

PurchasesBlock.propTypes = {
    profileSlug: PropTypes.string.isRequired,
    associationSlug: PropTypes.string.isRequired
};

export default PurchasesBlock;