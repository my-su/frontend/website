import PropTypes from "prop-types";
import React from "react";

import SpecificDataInfoForm from "../../../../Forms/InfoForms/SpecificData/SpecificDataInfoForm";

const DataFieldsBlock = ({ association, membership, is_you }) => {
    return (
        <SpecificDataInfoForm
            isYou={is_you}
            association={association}
            initialInfoOrFormState={"info"}
            specificData={membership.association_specific_data}
            showInfoFormStateButton={true}
            associationSlug={association.slug}
        />
    );
};

DataFieldsBlock.propTypes = {
    is_you: PropTypes.bool.isRequired,
    association: PropTypes.object.isRequired,
    membership: PropTypes.object.isRequired,
};

export default DataFieldsBlock;