import Typography from "@mui/material/Typography";
import * as moment from "moment/moment";
import PropTypes from "prop-types";
import React, {useMemo} from "react";

import Wrapper from "../../../../Components/FormComponents/Wrapper";
import Block from "../../../../Components/PageLayout/Content/Block";
import Divider from "../../../../Components/PageLayout/Content/Divider";
import ExtremeTable from "../../../../Components/Tables/ExtremeTable";
import {useGetEnrollmentsByAssociationAndProfileQuery} from "../../../../Store/services/MySU/enrollments";
import {useGetEventsByAssociationQuery} from "../../../../Store/services/MySU/events";

const enrollments_headers = [
    {name: "event", title: "Event"},
    {name: "start_date", title: "Start of event"},
    {name: "end_date", title: "End of event"},
    {name: "enrollment_option", title: "Enrollment option"},
    {name: "participation_fee", title: "Participation fee"}
];
const currencyColumns = ["participation_fee"];

const EventsBlock = ({ profileSlug, associationSlug }) => {
    const { data: events } = useGetEventsByAssociationQuery({ associationSlug: associationSlug, hidden: false});
    const { data: enrollments } = useGetEnrollmentsByAssociationAndProfileQuery({ profileSlug: profileSlug, associationSlug: associationSlug });

    const enrollments_rows = useMemo(()=>enrollments?.map(enrollment=>({
        slug: enrollment.slug,
        event: events?.find(event=> event.url === enrollment.event)?.name,
        start_date: moment.utc(enrollment.event.start_date).format("LLL"),
        end_date: moment.utc(enrollment.event.end_date).format("LLL"),
        enrollment_option: enrollment.enrollment_option.name,
        participation_fee: enrollment.enrollment_option.participation_fee,
    })),[enrollments, events]);


    return (
        <Block>
            <ExtremeTable
                rows={enrollments_rows || []}
                headers={enrollments_headers}
                showSelect={false}
                showGrouping={false}
                showColumnChooser={false}
                currencyColumns={currencyColumns}
                width={200}
                title={"Events"}
            />
        </Block>
    );
};

EventsBlock.propTypes = {
    profileSlug: PropTypes.string.isRequired,
    associationSlug: PropTypes.string.isRequired
};

export default EventsBlock;