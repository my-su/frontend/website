import {Container} from "@mui/material";
import Typography from "@mui/material/Typography";
import React from "react";

import Block from "../../Components/PageLayout/Content/Block";

const CampusCard = (props) => {
    return (
        <Container>
            <Block>
                <Typography variant={"h4"}>Campus Card</Typography>
                <hr className={"box-title-separator"}/>
                <Typography>Explantion of campus card</Typography>
                <br/><br/>
                <Typography>List of campus card options with payment link</Typography>
            </Block>

            <Block>
                <Typography variant={"h4"}>'verenigingsheffing'</Typography>
                <hr className={"box-title-separator"}/>
                <Typography>Explantion of 'verenigingsheffing'</Typography>
                <br/><br/>
                <Typography>List of 'verenigingsheffing' to be paid with payment link</Typography>
                <Typography>Don't create links without having a campus card</Typography>
            </Block>
        </Container>
    );
};

export default CampusCard;