import React from "react";
import {useTimer} from "react-timer-hook";

import Button from "../../../../Components/Buttons/Button";

const TimedButton = ({ buttonProps, children, time, onExpire }) => {
    const { seconds, isRunning } = useTimer({ expiryTimestamp: time, onExpire: onExpire });
    return (
        <Button {...buttonProps}>
            <span>{ isRunning && seconds }</span>
            { children }
        </Button>
    );
};

export default TimedButton;