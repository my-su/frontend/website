import { Button, Grid } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
    container: {
        marginLeft: 0,
        marginRight: 0,
        width: "100%"
    },
    item: {
        flexGrow: 1
    },
    numberRow: {
        flexGrow: 1
    },
    button: {
        width: "100%"
    },
}));

const MuiNumpad = (props) => {
    const {
        onChange,
        onKeyPress,
    } = props;

    const classes = useStyles();

    const [inputValue, setInputValue] = useState("");

    const handleOnChange = (_value) => {
        const newValue = inputValue.concat(_value);
        setInputValue(newValue);
        onChange(newValue);
    };

    const onButtonPress = (_value) => {
        if (onChange) handleOnChange(_value);
        if (onKeyPress) onKeyPress(_value);
    };

    const handleClear = () => {
        setInputValue("");
        onChange("");
    };

    const handleDelete = () => {
        const newString = inputValue.substring(0, inputValue.length - 1);
        setInputValue(newString);
        onChange(newString);
    };

    return (
        <Grid container className={classes.container} spacing={1}>
            <Grid item xs={12}>
                <Grid container justifyContent={"center"} spacing={1}>
                    { [1, 2, 3].map((value) => (
                        <Grid key={value} item className={classes.item}>
                            <Button onClick={() => onButtonPress(value)} color={"primary"} variant={"contained"} className={classes.button}>{ value }</Button>
                        </Grid>
                    )) }
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container justifyContent={"center"} spacing={1}>
                    { [4, 5, 6].map((value) => (
                        <Grid key={value} item className={classes.item}>
                            <Button onClick={() => onButtonPress(value)} color={"primary"} variant={"contained"} className={classes.button}>{ value }</Button>
                        </Grid>
                    )) }
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container justifyContent={"center"} spacing={1}>
                    { [7, 8, 9].map((value) => (
                        <Grid key={value} item className={classes.item}>
                            <Button onClick={() => onButtonPress(value)} color={"primary"} variant={"contained"} className={classes.button}>{ value }</Button>
                        </Grid>
                    )) }
                </Grid>
            </Grid>
            <Grid item xs={12} className={classes.numberRow} >
                <Grid container justifyContent={"center"} className={classes.numberRow} spacing={1}>
                    <Grid key={"dot"} item className={classes.item}>
                        <Button disabled={!inputValue.length} onClick={handleDelete} color={"secondary"} variant={"contained"} className={classes.button}>&larr;</Button>
                    </Grid>
                    <Grid key={0} item className={classes.item}>
                        <Button onClick={() => onButtonPress(0)} color={"primary"} variant={"contained"} className={classes.button}>0</Button>
                    </Grid>
                    <Grid key={"clear"} item className={classes.item}>
                        <Button disabled={!inputValue.length} onClick={handleClear} variant={"contained"} className={classes.button}>C</Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

MuiNumpad.propTypes = {
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
};

MuiNumpad.defaultProps = {
    onChange: undefined,
    onKeyPress: undefined,
};

export default MuiNumpad;