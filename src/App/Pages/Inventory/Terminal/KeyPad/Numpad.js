import { Grid, TextField } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import React, { useState } from "react";

// import MuiNumpad from "mui-numpad";
import MuiNumpad from "./MuiNumpad";

const useStyles = makeStyles((theme) => ({
    gridItem: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
    },
    textField: {
        width: "100%",
    }
}));

const SimpleNumpad = () => {
    const classes = useStyles();
    const [value, setValue] = useState();

    const onChange = (_value) => {
        setValue(_value);
    };

    return (
        <>
            <TextField
                margin={"dense"}
                className={classes.textField}
                value={value}
                variant={"outlined"}
                placeholder={"Numpad presses"}
            />
            <MuiNumpad
                onChange={onChange}
            />
        </>
    );
};

export default SimpleNumpad;