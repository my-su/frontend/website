import {Typography} from "@mui/material";
import Container from "@mui/material/Container";
import React, {useState} from "react";
import {useParams} from "react-router-dom";

import {BackButtonV2} from "../../Components/BackButton";
import Button from "../../Components/Buttons/Button";
import Wrapper from "../../Components/FormComponents/Wrapper";
import Block from "../../Components/PageLayout/Content/Block";
import RoutingButton from "../../Components/PageLayout/Content/RoutingButton";
import EventTypeInfo from "../../Forms/DropdownForms/EventTypes/EventTypeInfo";
import EventTypesDropDown, {EventTypesMultiForm} from "../../Forms/DropdownForms/EventTypes/EventTypesDropDown";
import {useGetEventTypesByAssociationQuery} from "../../Store/services/MySU/eventTypes";


const EventTypes = () => {
    const { slug } = useParams();
    const { data: eventTypes } = useGetEventTypesByAssociationQuery(slug);

    const [creating, setCreating] = useState(false);

    return (
        <Container maxWidth={"xl"}>
            <Wrapper>
                <RoutingButton
                    routeStringPieces={[
                        "Events", "Event Types"
                    ]}
                />
                <BackButtonV2 container={false}/>
            </Wrapper>
            <Block>
                <EventTypesMultiForm
                    eventTypes={eventTypes || []}
                    associationUrl={`/associations/${slug}`}
                    creating={creating}
                    postSubmit={()=>setCreating(false)}
                    postCancel={()=>setCreating(false)}
                />
            </Block>
        </Container>
    );

};

EventTypes.propTypes = {
};

EventTypes.defaultProps = {
};

export default EventTypes;
