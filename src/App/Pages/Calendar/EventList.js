import AddIcon from "@mui/icons-material/Add";
import SettingsIcon from "@mui/icons-material/Settings";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import * as moment from "moment/moment";
import React, {useState} from "react";
import {useHistory, useParams} from "react-router-dom";

import {BackButtonV2} from "../../Components/BackButton";
import Button from "../../Components/Buttons/Button";
import Wrapper from "../../Components/FormComponents/Wrapper";
import useModalState from "../../Components/Hooks/useModalState";
import RoutingButton from "../../Components/PageLayout/Content/RoutingButton";
import TopPageBlock from "../../Components/PageLayout/Content/TopPageBlock";
import EventModalForm from "../../Forms/ModalForms/Event/EventModalForm";
import useIsBoardMemberPage from "../../Store/hooks/useIsBoardMemberPage";
import {useGetEnrollmentOptionsByEventQuery} from "../../Store/services/MySU/enrollmentOptions";
import {useGetEnrollmentsByEventQuery} from "../../Store/services/MySU/enrollments";
import {useGetEventQuery, useGetEventsByAssociationQuery} from "../../Store/services/MySU/events";
import {useGetEventTypesByAssociationQuery} from "../../Store/services/MySU/eventTypes";
import EventCard from "../MyAssociation/Home/Cards/EventCard";
import EventModal from "../MyAssociation/Home/Modals/EventModal";
import {fromAPItoSchedulerFormat} from "./Scheduler";

const sortFilter = (events) => {
    if (events !== undefined) {
        console.log(events);
        const sorted = [...events, []];
        sorted.sort((a, b) => (a.start_date > b.start_date) ? 1 : -1);
        const now = moment.utc().local().format();
        return sorted.filter((a) => a.end_date > now);
    }
    return [];
};

const EventList = () => {
    const { slug } = useParams();
    const history = useHistory();
    const isBoardPage = useIsBoardMemberPage();
    const [eventSlug, setEventSlug] = useState();
    const { data: eventTypes } = useGetEventTypesByAssociationQuery(slug);

    const [selectedEventItem, setSelectedEventItem] = useState("");
    const onEventsClick = (eventSlug) => () => setSelectedEventItem(eventSlug);
    const onResetEvent = () => setSelectedEventItem("");


    const resetEventSlug = () => {
        setEventSlug("");
    };

    const { data: eventsMember } = useGetEventsByAssociationQuery({associationSlug: slug, hidden: isBoardPage && ""});
    const { data: events } = useGetEventsByAssociationQuery(slug);
    const filtered = sortFilter(events);
    const filteredMember = sortFilter(eventsMember);

    return (
        <Container maxWidth={"xl"}>
            <EventModalForm
                visible={!!eventSlug}
                postCancel={resetEventSlug}
                postSubmit={resetEventSlug}
                postDelete={resetEventSlug}
                eventTypes={eventTypes || []}
                eventSlug={eventSlug}
                associationSlug={slug}
            />
            <EventModal
                slug={selectedEventItem}
                onClose={onResetEvent}
            />
            <Wrapper>
                <RoutingButton
                    routeStringPieces={[
                        "Events"
                    ]}
                />
            </Wrapper>
            <TopPageBlock>
                <Grid container spacing={2} justifyContent={"flex-start"} alignItems={"center"}>
                    <Grid item>
                        <Typography variant={"h5"}>
                            Upcoming events
                        </Typography>
                    </Grid>
                    { isBoardPage &&
                        <>
                            <Grid item sx={{flexGrow: 1}}>
                                <Button variant={"contained"} size={"small"} style={{textTransform: "none"}}
                                    startIcon={<AddIcon fontSize={"large"}/>} onClick={() => setEventSlug("new")}
                                >
                                    Add
                                </Button>
                            </Grid>
                            <Grid item sx={{alignSelf: "flex-end"}}>
                                <Button variant={"outlined"} size={"small"} style={{textTransform: "none"}} startIcon={<SettingsIcon fontSize={"large"} />} onClick={()=> history.push("./events/types")}>
                                Event Types
                                </Button>
                            </Grid>
                        </>
                    }
                </Grid>
            </TopPageBlock>
            <Box sx={{ mt: 2 }}>
                <Grid container spacing={4} justifyContent={"flex-start"} alignItems={"flex-start"}>
                    { isBoardPage &&
                        filtered?.map(event=>(
                            <Grid item key={event.slug}>
                                <EventCard {...event} onClick={() => setEventSlug(event.slug)}/>

                            </Grid>
                        ))
                    }
                    { !isBoardPage &&
                        filteredMember?.map(event=>(
                            <Grid item key={event.slug}>
                                <EventCard key={event.slug} onClick={onEventsClick(event.slug)} {...event}/>
                            </Grid>
                        ))
                    }
                </Grid>
            </Box>
        </Container>
    );
};

EventList.propTypes = {
};

export default EventList;