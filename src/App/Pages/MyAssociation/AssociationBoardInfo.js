import "react-simple-tree-menu/dist/main.css";

import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { orderBy } from "lodash";
import * as moment from "moment";
import React from "react";
import {useParams} from "react-router-dom";

import Image from "../../../img/default_photo.jpg";
import Sushi from "../../../img/Sushi-11-me.jpg";
import CardGrid from "../../Components/Card/CardGrid";
import ContactCard from "../../Components/Card/ContactCard";
import Wrapper from "../../Components/FormComponents/Wrapper";
import Loading from "../../Components/Loading";
import Block from "../../Components/PageLayout/Content/Block";
import RoutingButton from "../../Components/PageLayout/Content/RoutingButton";
import Info from "../../Forms/InfoForms/Info";
import {useGetCurrentBoardOfAssociationQuery} from "../../Store/services/MySU/groups";


const BoardCard = ({ photo, full_name, duty, email }) => {
    return (
        <Card sx={{ width: 384, height: 451, borderRadius: 4}}>
            <CardMedia
                sx={{ width: 304, height: 304, margin: "auto", mt: 5, borderRadius: 4 }}
                image={photo || Image}
            />
            <CardContent>
                <Typography variant={"h6"}>{ full_name }</Typography>
                <Typography>{ duty } | { email }</Typography>
            </CardContent>
        </Card>
    );
};

const morphBoardMemberToCard = (board_member) => {
    let data = {
        "Name": board_member.full_name,
    };
    if (board_member.email) {
        data["Email"] = board_member.email;
    }
    return {
        duty: board_member.duty,
        data: data,
        description: board_member.description,
        photo: null,
        order: board_member.order
    };
};

const BoardInfo = (slug) => {
    // const { slug } = useParams();
    const { data: board, loading } = useGetCurrentBoardOfAssociationQuery(slug);

    if (loading || !board) {
        return (
            <Container maxWidth={"xl"}>
                <Wrapper>
                    <RoutingButton
                        routeStringPieces={[
                            "Association", "Current board"
                        ]}
                    />
                </Wrapper>
                <Loading/>
            </Container>
        );
    }

    // const boardCards = orderBy(board?.groupmemberships.map(board_member=>morphBoardMemberToCard(board_member)), ["order"], ["asc"]);
    const boardCards = orderBy(board?.groupmemberships, ["order"], ["asc"]);

    return (
        <Container maxWidth={"xl"}>
            <Wrapper>
                <RoutingButton
                    routeStringPieces={[
                        "Association", "Current board"
                    ]}
                />
            </Wrapper>
            <Block>
                <Typography variant={"h5"}>{ board.full_name }</Typography>
            </Block>
            <Grid container spacing={4} sx={{ mt: 0 }}>
                <Grid item xs={7.7}>
                    <Block>
                        <Box
                            component={"img"}
                            sx={{
                                borderRadius: 4,
                                width: "100%",
                                maxHeight: 432
                            }}
                            src={board.photo || Sushi}
                            alt={"Photo of board"}
                        />
                        <Typography>
                            { board.description }
                        </Typography>
                    </Block>
                </Grid>
                <Grid item xs={4}>
                    <Block>
                        <Info
                            headerless={false}
                            data={{
                                "CONTACT": {
                                    "Name": board.full_name,
                                    "Short name": board.short_name,
                                    "Email": board.email,
                                },
                                "PERSONAL": {
                                    "Creed": board.creed,
                                },
                                "CONSTITUTIONAL": {
                                    "Year": board.year,
                                    "Number": board.number,
                                    "From": moment(board.founding_date).format("LL"),
                                    "To": board.dissolution_date ? moment(board.dissolution_date).format("LL") : "-"
                                }
                            }}
                        />
                    </Block>
                </Grid>
            </Grid>
            <CardGrid
                componentProps={boardCards}
                Component={BoardCard}
                // itemGrowth={1}
                // direction={"column"}
            />
        </Container>
    );
};

BoardInfo.propTypes = {
};

export default BoardInfo;