import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import {Container, Typography} from "@mui/material";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import makeStyles from "@mui/styles/makeStyles";
import moment from "moment";
import PropTypes from "prop-types";
import React from "react";
import { loremIpsum } from "react-lorem-ipsum";
import {useHistory, useParams} from "react-router-dom";

import Button from "../../../Components/Buttons/Button";
import Wrapper from "../../../Components/FormComponents/Wrapper";
import useModalState from "../../../Components/Hooks/useModalState";
import ConfirmationModal from "../../../Components/Modals/ConfirmationModal";
import Block from "../../../Components/PageLayout/Content/Block";
import FigmaContent from "../../../Components/PageLayout/Content/FigmaContent";
import {useGetNewsByAssociationQuery} from "../../../Store/services/MySU/news";
import {useDeleteMembershipMutation} from "../../../Store/services/MySU/user";

const useStyles = makeStyles(theme => ({
    eventsBlock: {
        background: theme.palette.background.paper,
        width: "60%",
        margin: "auto",
        marginTop: "32px"
    },
    eventsList: {
        padding: theme.spacing(2)
    },
    moreEventsButton: {
        height: "40px",
        width: "100%",
        color: theme.palette.common.white,
        backgroundColor: theme.palette.primary.dark
    },
    introText: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(6),
    },
    container: {

    }
}));

const EventBlock = ({ event }) => {
    return (
        <Grid container spacing={2} component={Button} onClick={()=>console.log("clicked!")}>
            <Grid item xs={2}>
                <Typography variant={"h5"}>15</Typography>
                <Typography>Feb</Typography>
            </Grid>
            <Grid item xs={9}>
                <Typography variant={"h5"}>Ice Skating</Typography>
            </Grid>
            <Grid item xs={1}>
                <KeyboardArrowRightIcon fontSize={"large"}/>
            </Grid>
        </Grid>
    );
};

const NewsBlock = ({ news }) => {
    return (
        <Grid container spacing={2} direction={"column"}>
            <Grid item xs={2}>
                <Wrapper>
                    <Typography variant={"h5"}>{ news?.title || "Lorem Ipsum" }</Typography>
                    <Typography>{ moment(news?.date).format("LLLL") || "Feb 15th 2022" }</Typography>
                </Wrapper>
            </Grid>
            <Grid item xs={9}>
                <Typography>{ news?.text || loremIpsum() }</Typography>
            </Grid>
        </Grid>
    );
};

NewsBlock.defaultProps = {
};

const Member = ({ association, membership }) => {
    const classes = useStyles();
    const history = useHistory();
    const { slug } = useParams();

    const { data: news } = useGetNewsByAssociationQuery(slug);
    
    const [modalOpen, toggleModalOpen] = useModalState(false);
    const [cancelMembership] = useDeleteMembershipMutation();

    const onCancelButtonClick = () => toggleModalOpen();
    const onCancel = () => toggleModalOpen();
    const onConfirm = () => {
        toggleModalOpen();
        cancelMembership(membership.slug).then(()=>history.push("/protected/associations/overview"));
    };

    if (membership.status === "Pending") {
        return (
            <FigmaContent
                name={association.name}
                image={null}
            >
                <Container>
                    <ConfirmationModal
                        title={"Cancel Membership Request"}
                        description={"Are you sure you want to cancel your request?"}
                        open={modalOpen}
                        onCancel={onCancel}
                        onConfirm={onConfirm}
                    />
                    <Block>
                        <Wrapper>
                            <Typography variant={"h5"}>Membership notice</Typography>
                            <Button color={"secondary"} variant={"outlined"} onClick={onCancelButtonClick}>Cancel Request</Button>
                        </Wrapper>
                        <Divider/>
                        <br/>
                        <Typography>Your membership has not yet been accepted. Please wait until the board has accepted.</Typography>
                    </Block>
                </Container>
            </FigmaContent>
        );
    }

    return (
        <FigmaContent
            name={association.name}
            image={null}
        >
            <Grid container>
                <Grid item xs={1}/>
                <Grid item xs={6}>
                    <div className={classes.container}>
                        <div className={classes.introText}>
                            <Typography>A warm welcom to { association.name }. Want to know more? See Association&gt;About.</Typography>
                        </div>
                        <Divider>
                            <Typography variant={"h4"}>NEWS</Typography>
                        </Divider>
                        { news?.map((newsItem, id)=>
                            <NewsBlock key={id} news={newsItem}/>
                        ) }
                        <NewsBlock/>
                        <NewsBlock/>
                    </div>
                </Grid>
                <Grid item xs={5}>
                    <div className={classes.eventsBlock}>
                        <div className={classes.eventsList}>
                            <EventBlock/>
                            <EventBlock/>
                            <EventBlock/>
                            <EventBlock/>
                            <EventBlock/>
                            <EventBlock/>
                        </div>
                        <Button className={classes.moreEventsButton}>
                            <Typography variant={"h6"} style={{ float: "right" }}>More events &gt;</Typography>
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </FigmaContent>
    );
};

Member.propTypes = {
    association: PropTypes.object.isRequired,
    membership: PropTypes.object.isRequired
};

export default Member;