import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";
import { loremIpsum } from "react-lorem-ipsum";

import FigmaContent from "../../Components/PageLayout/Content/FigmaContent";
import AssociationInfo from "../../Forms/InfoForms/Association/AssociationInfo";
import {useCurrentAssociation} from "../../Store/services/MySU/associations";

const useStyles = makeStyles(theme => ({
    aboutText: {
        paddingTop: theme.spacing(3),
    },
    tablePaper: {
        background: theme.palette.background.paper,
        width: "75%",
        margin: "auto",
        marginTop: theme.spacing(3),
        padding: theme.spacing(3),
    }
}));

const About = () => {
    const classes = useStyles();
    const {data: association } = useCurrentAssociation();

    return (
        <FigmaContent
            image={null}
            name={association?.name || ""}
        >
            <Grid container>
                <Grid item xs={1}/>
                <Grid item xs={6}>
                    <div className={classes.aboutText}>
                        <Typography variant={"h4"}>
                            About
                        </Typography>
                        <Divider/>
                        <Typography>
                            Welcome on the official MySU website of Foton Arts. We are a visual arts association of the University of Twente.
                        </Typography>
                        <br/>
                        <Typography>
                            { loremIpsum({p: 2}) }
                        </Typography>
                        <br/>
                        <Typography>
                            { loremIpsum({p: 2}) }
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={5}>
                    <Paper className={classes.tablePaper} elevation={0}>
                        <AssociationInfo association={association}/>
                    </Paper>
                </Grid>
            </Grid>
        </FigmaContent>
    );
};

About.propTypes = {
};

About.defaultProps = {
};

export default About;
