import {Skeleton} from "@mui/material";
import Box from "@mui/material/Box";
import MuiCardMedia from "@mui/material/CardMedia";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import React from "react";

import Image from "../../../img/default_photo.jpg";
import Wrapper from "../../Components/FormComponents/Wrapper";
import Block from "../../Components/PageLayout/Content/Block";
import RoutingButton from "../../Components/PageLayout/Content/RoutingButton";
import TopPageBlock from "../../Components/PageLayout/Content/TopPageBlock";
import AssociationInfo from "../../Forms/InfoForms/Association/AssociationInfo";
import {useCurrentAssociation} from "../../Store/services/MySU/associations";

const About = () => {
    const { data: association } = useCurrentAssociation();

    return (
        <Container maxWidth={"xl"}>
            <Wrapper>
                <RoutingButton
                    routeStringPieces={[
                        "Association", "About"
                    ]}
                />
            </Wrapper>
            <TopPageBlock>
                <Typography variant={"h5"}>
                    { association ? association.name : <Skeleton /> }
                </Typography>
            </TopPageBlock>
            <Grid container rowSpacing={4} spacing={6} sx={{ mt: 0 }}>
                <Grid item xs={8}>
                    <Block>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant={"h5"}>
                                    About us
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Divider/>
                            </Grid>
                            <Grid item xs={12}>
                                <MuiCardMedia image={association ? association.photo : Image}
                                    sx={{
                                        width: "100%",
                                        height: 0,
                                        paddingBottom: "48%",
                                        borderRadius: 4,
                                        backgroundColor: "background.paper",
                                        position: "relative",
                                        transform: "translateY(8px)",
                                        // paddingLeft: 1,
                                        // paddingRight: 1,
                                        "&:after": {
                                            content: "\" \"",
                                            position: "absolute",
                                            top: 0,
                                            left: 0,
                                            width: "100%",
                                            height: "100%",
                                            borderRadius: "shape.borderRadius",
                                            opacity: 0.5,
                                        },
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Typography>
                                    { association ? association.description : <Skeleton /> }
                                </Typography>
                            </Grid>
                        </Grid>
                    </Block>
                </Grid>
                <Grid item xs={4}>
                    <Block>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant={"h5"}>Details</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Divider/>
                            </Grid>
                            <Grid item xs={12}>
                                <AssociationInfo association={association}/>
                            </Grid>
                        </Grid>
                    </Block>
                </Grid>
            </Grid>
        </Container>
    );
};

About.propTypes = {
};

About.defaultProps = {
};

export default About;
