import Container from "@mui/material/Container";
import LinearProgress from "@mui/material/LinearProgress";
import Typography from "@mui/material/Typography";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";
import React from "react";
import {ValidatorForm} from "react-material-ui-form-validator";
import {useParams} from "react-router-dom";

import Button from "../../Components/Buttons/Button";
import TextField from "../../Components/FormComponents/Fields/TextField";
import Wrapper from "../../Components/FormComponents/Wrapper";
import Block from "../../Components/PageLayout/Content/Block";
import {useAddNewsMutation, useDeleteNewsMutation, useGetNewsByAssociationQuery} from "../../Store/services/MySU/news";

const useStyles = makeStyles((theme) => ({
    progress: {
        width: "100%",
        "& > * + *": {
            marginTop: theme.spacing(2),
        },
    },
}));

const News = () => {
    const classes = useStyles();

    const { slug } = useParams();
    const { data: news, isLoading } = useGetNewsByAssociationQuery(slug);
    const [ addNews ] = useAddNewsMutation();
    const [ deleteNews ] = useDeleteNewsMutation();

    const handleSubmit = (event) => {
        const data = new FormData(event.target);
        addNews({
            title: data.get("title"),
            text: data.get("text"),
            association: `/associations/${slug}`
        });
    };

    if (isLoading) {
        return (
            <LinearProgress className={classes.progress}/>
        );
    }

    return (
        <Container>
            <Block>
                <ValidatorForm
                    onSubmit={handleSubmit}
                    onError={errors => console.log(errors)}
                >
                    <TextField
                        name={"title"}
                    />
                    <TextField
                        name={"text"}
                    />
                    <Button type={"submit"} variant={"contained"} color={"primary"}>Save</Button>
                </ValidatorForm>
            </Block>
            { news.length === 0 ? (
                <Block>
                    <Typography variant={"h5"}>News</Typography>
                    <hr className={"box-title-separator"}/>
                    <Typography>There is no news</Typography>
                </Block>
            ) : (news.map((news_item, id) => (
                <Block key={id}>
                    <Wrapper>
                        <Typography variant={"h5"}>{ news_item.title }</Typography>
                        <Button onClick={()=>deleteNews(news_item.slug)} color={"secondary"}>Delete</Button>
                    </Wrapper>
                    <hr className={"box-title-separator"}/>
                    <Typography variant={"h5"}>{ news_item.text }</Typography>
                </Block>
            ))) }
        </Container>
    );
};

export default News;