import {emptySplitApi, pessimisticCreation} from "./index";

const eventTypesApi = emptySplitApi
    .enhanceEndpoints({addTagTypes: ["EventType"]})
    .injectEndpoints({
        endpoints: (build) => ({
            getEventTypesByAssociation: build.query({
                query: (association_slug) => ({ url: "/event_types", params: {"association__slug": association_slug, limit: 100} }),
                providesTags: (result, error, association_slug) => [{ type: "EventType", id: association_slug }],
                transformResponse: (response) => response.results
            }),
            addEventType: build.mutation({
                query: (body) => ({ url: "/event_types/", method: "POST", body: body }),
                async onQueryStarted(eventType, { dispatch, queryFulfilled }) {
                    await pessimisticCreation({
                        dispatch,
                        queryFulfilled,
                        api: eventTypesApi,
                        query: "getEventTypesByAssociation",
                        entityToQueryParam: () => eventType.association.split("/associations/")[1]
                    });
                }
            }),
            patchEventType: build.mutation({
                query: ({ slug, ...patch }) => ({ url: `/event_types/${slug}`, method: "PATCH", body: patch }),
                invalidatesTags: ({ association }) => [{ type: "EventType", id: association.split("/associations/")[1] }]
            }),
            deleteEventType: build.mutation({
                query: ({ slug }) => ({ url: `/event_types/${slug}`, method: "DELETE" }),
                invalidatesTags: ({ association }) => [{ type: "EventType", id: association.split("/associations/")[1] }]
            })
        }),
        overrideExisting: false
    });

export const {
    useGetEventTypesByAssociationQuery,
    useAddEventTypeMutation,
    usePatchEventTypeMutation,
    useDeleteEventTypeMutation,
} = eventTypesApi;
export default eventTypesApi;