import {emptySplitApi} from "./index";

const purchasesApi = emptySplitApi
    .enhanceEndpoints({addTagTypes: ["Purchase"]})
    .injectEndpoints({
        endpoints: (build) => ({
            getPurchasesByAssociationAndProfile: build.query({
                query: ({ profileSlug, associationSlug }) => ({ url: "/purchases", params: { profile__slug: profileSlug, association__slug: associationSlug } }),
                providesTags: (result, error, { profileSlug, associationSlug }) => [{ type: "Purchase", id: `${profileSlug}__${associationSlug}` }],
                transformResponse: (response) => response.results
            }),
            getPurchasesByAssociation: build.query({
                query: (associationSlug) => ({ url: "/purchases", params: {"association__slug": associationSlug, limit: 1000} }),
                providesTags: (result, error, associationSlug) => [{ type: "Purchase", id: associationSlug }],
                transformResponse: (response) => response.results
            }),
        }),
        overrideExisting: false
    });

export const {
    useGetPurchasesByAssociationAndProfileQuery,
    useGetPurchasesByAssociationQuery,
} = purchasesApi;
export default purchasesApi;