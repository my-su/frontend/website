import {useLocation} from "react-router-dom";

const useIsBoardMemberPage = () => {
    const { pathname } = useLocation();
    return pathname.includes("/boardmember/");
};

export default useIsBoardMemberPage;