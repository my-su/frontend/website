// function PostsList() {
//     const { data: posts } = api.useGetPostsQuery()
//
//     return (
//         <ul>
//             {posts?.data?.map((post) => (
//                 <PostById key={post.id} id={post.id} />
//             ))}
//         </ul>
//     )
// }
//
// function PostById({ id }: { id: number }) {
//     // Will select the post with the given id, and will only rerender if the given posts data changes
//     const { post } = api.useGetPostsQuery(undefined, {
//         selectFromResult: ({ data }) => ({
//             post: data?.find((post) => post.id === id),
//         }),
//     })
//
//     return <li>{post?.name}</li>
// }
