import {useEffect} from "react";
import {useDispatch} from "react-redux";

import {emptySplitApi} from "../services/MySU";

// taken from https://redux-toolkit.js.org/rtk-query/usage/prefetching and adapted to used conventions
const usePrefetchImmediately = (endpoint, arg, options) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(emptySplitApi.util.prefetch(endpoint, arg, options));
    }, []);
};

// In a component
// usePrefetchImmediately("getUser", 5);

export default usePrefetchImmediately;