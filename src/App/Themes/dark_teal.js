import { adaptV4Theme,createTheme } from "@mui/material/styles";

const darkTealTheme = createTheme(adaptV4Theme({
    palette: {
        mode: "dark",
        primary: {
            main: "#92abcf"
        },
        secondary: {
            main: "rgb(17, 236, 229)"
        }
    }
}));

export default darkTealTheme;