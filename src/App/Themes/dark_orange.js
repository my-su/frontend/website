import {deepOrange, orange} from "@mui/material/colors";
import { adaptV4Theme,createTheme } from "@mui/material/styles";

const darkOrangeTheme = createTheme(adaptV4Theme({
    palette: {
        mode: "dark",
        primary: {
            main: orange[500]
        },
        secondary: {
            main: deepOrange[900]
        }
    }
}));

export default darkOrangeTheme;