import { adaptV4Theme, createTheme } from "@mui/material/styles";

const darkTheme = createTheme(adaptV4Theme({
    palette: {
        mode: "dark",
    }
}));

export default darkTheme;